-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2018 at 04:36 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `data-penduduk`
--

-- --------------------------------------------------------

--
-- Table structure for table `bidang_usaha`
--

CREATE TABLE IF NOT EXISTS `bidang_usaha` (
`id_bidang_usaha` int(11) NOT NULL,
  `bidang_usaha` varchar(50) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `bidang_usaha`
--

INSERT INTO `bidang_usaha` (`id_bidang_usaha`, `bidang_usaha`) VALUES
(1, 'Perkebunan'),
(2, 'Perikanan'),
(3, 'Peternakan'),
(4, 'Kontruksi'),
(5, 'Perdagangan');

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE IF NOT EXISTS `desa` (
`id_desa` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `nama_desa` varchar(30) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`id_desa`, `id_kecamatan`, `nama_desa`) VALUES
(1, 1, 'Kandangan Lama'),
(2, 3, 'Pelaihari');

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE IF NOT EXISTS `kabupaten` (
`id_kabupaten` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `nama_kabupaten` varchar(30) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`id_kabupaten`, `id_provinsi`, `nama_kabupaten`) VALUES
(1, 1, 'Tanah Laut');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE IF NOT EXISTS `kecamatan` (
`id_kecamatan` int(11) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `id_kabupaten`, `nama_kecamatan`) VALUES
(1, 1, 'Panyipatan'),
(3, 1, 'Pelaihari');

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE IF NOT EXISTS `pekerjaan` (
`id_pekerjaan` int(11) NOT NULL,
  `nama_pekerjaan` varchar(30) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id_pekerjaan`, `nama_pekerjaan`) VALUES
(1, 'PNS'),
(2, 'Petani/Pekebun'),
(3, 'Buruh Lepas'),
(4, 'Tukang'),
(5, 'Honorer'),
(6, 'Karyawan Swasta'),
(7, 'Mengurus Rumah Tangga'),
(8, 'Belum/Tidak Bekerja'),
(9, 'Pelajar/Mahasiswa'),
(10, 'Wiraswasta');

-- --------------------------------------------------------

--
-- Table structure for table `pendataan`
--

CREATE TABLE IF NOT EXISTS `pendataan` (
`id_pendataan` int(11) NOT NULL,
  `id_penduduk` int(11) NOT NULL,
  `parameter` varchar(20) NOT NULL,
  `pendataan` varchar(50) NOT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `keterangan2` varchar(100) DEFAULT NULL,
  `keterangan3` varchar(100) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `id1` int(11) DEFAULT NULL,
  `id2` int(11) DEFAULT NULL,
  `status_pendataan` varchar(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `pendataan`
--

INSERT INTO `pendataan` (`id_pendataan`, `id_penduduk`, `parameter`, `pendataan`, `keterangan`, `keterangan2`, `keterangan3`, `tanggal`, `id1`, `id2`, `status_pendataan`, `timestamp`) VALUES
(1, 1, 'status', 'Kawin', 'tesd', '', '', '2018-05-12', NULL, NULL, 'Perubahan', '2018-05-20 13:42:18'),
(39, 56, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-03-11', NULL, NULL, 'Baru', '2018-03-11 00:31:09'),
(38, 53, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-03-11', NULL, NULL, 'Baru', '2018-03-11 00:24:06'),
(8, 1, 'sudahektp', 'Kantor Kelurahan', '2018-05-20', NULL, '', '2018-05-20', NULL, NULL, 'Perubahan', '2018-05-20 14:14:58'),
(35, 46, 'kelahiran', 'Normal', 'TITIK P.', 'Rumah Bidan', NULL, '2018-04-11', NULL, NULL, 'Baru', '2018-04-10 23:58:53'),
(36, 49, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-04-11', NULL, NULL, 'Baru', '2018-04-11 00:15:29'),
(37, 50, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-04-11', NULL, NULL, 'Baru', '2018-04-11 00:17:57'),
(14, 15, 'kematian', 'Sakit', 'Kuburan Muslimin', 'Tidak ada penyebab lainnya', NULL, '2018-05-29', NULL, NULL, 'Perubahan', '2018-05-29 06:23:06'),
(15, 1, 'kematian', '7777', '777', 'ffff', NULL, '2012-07-08', NULL, NULL, 'Perubahan', '2018-06-08 07:29:06'),
(34, 43, 'kelahiran', 'Normal', 'Eka Safitri', 'Rumah Bidan', NULL, '2018-05-11', NULL, NULL, 'Baru', '2018-05-10 23:43:31'),
(16, 18, 'kematian', 'Sakit', 'Kuburan Muslimin', 'tidak ada', NULL, '2018-07-04', NULL, NULL, 'Perubahan', '2018-07-04 07:38:16'),
(33, 42, 'kelahiran', 'Normal', 'Eka Safitri', 'Rumah Bidan', NULL, '2018-05-11', NULL, NULL, 'Baru', '2018-05-10 23:41:39'),
(26, 25, 'kelahiran', 'Normal', 'RAHMAWATI', 'Rumah Bidan', NULL, '2018-07-10', NULL, NULL, 'Baru', '2018-07-10 16:27:42'),
(27, 26, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-07-10', NULL, NULL, 'Baru', '2018-07-10 16:37:17'),
(28, 27, 'kelahiran', 'Normal', 'TITIK P.', 'Rumah Bidan', NULL, '2018-07-10', NULL, NULL, 'Baru', '2018-07-10 16:39:38'),
(29, 28, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-07-10', NULL, NULL, 'Baru', '2018-07-10 16:48:40'),
(30, 29, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-07-10', NULL, NULL, 'Baru', '2018-07-10 16:50:07'),
(32, 39, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-05-11', NULL, NULL, 'Baru', '2018-05-10 23:31:10'),
(40, 59, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-03-11', NULL, NULL, 'Baru', '2018-03-11 00:35:58'),
(41, 60, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-02-11', NULL, NULL, 'Baru', '2018-02-11 00:40:50'),
(42, 61, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-02-11', NULL, NULL, 'Baru', '2018-02-11 00:41:54'),
(43, 62, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-02-11', NULL, NULL, 'Baru', '2018-02-11 00:43:42'),
(44, 65, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-01-11', NULL, NULL, 'Baru', '2018-01-11 00:50:49'),
(45, 66, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-01-11', NULL, NULL, 'Baru', '2018-01-11 00:53:10'),
(46, 69, 'kelahiran', '', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-01-11', NULL, NULL, 'Baru', '2018-01-11 00:58:48'),
(47, 72, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-08-11', NULL, NULL, 'Baru', '2018-08-11 01:04:44'),
(48, 75, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-08-11', NULL, NULL, 'Baru', '2018-08-11 01:09:59'),
(49, 78, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-08-11', NULL, NULL, 'Baru', '2018-08-11 01:15:28'),
(50, 79, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-08-11', NULL, NULL, 'Baru', '2018-08-11 01:16:51'),
(51, 82, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-09-11', NULL, NULL, 'Baru', '2018-09-11 01:26:13'),
(52, 87, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-09-11', NULL, NULL, 'Baru', '2018-09-11 01:38:20'),
(53, 90, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-09-11', NULL, NULL, 'Baru', '2018-09-11 01:44:51'),
(54, 93, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-10-11', NULL, NULL, 'Baru', '2018-10-11 02:18:26'),
(55, 94, 'kelahiran', '', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-10-11', NULL, NULL, 'Baru', '2018-10-11 02:19:31'),
(56, 97, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-10-11', NULL, NULL, 'Baru', '2018-10-11 02:24:21'),
(57, 98, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-10-11', NULL, NULL, 'Baru', '2018-10-11 02:25:34'),
(58, 101, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-11-11', NULL, NULL, 'Baru', '2018-11-11 02:32:29'),
(59, 102, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-11-11', NULL, NULL, 'Baru', '2018-11-11 02:33:59'),
(60, 105, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-11-11', NULL, NULL, 'Baru', '2018-11-11 02:40:58'),
(61, 108, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-11-11', NULL, NULL, 'Baru', '2018-11-11 02:46:01'),
(62, 111, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-12-11', NULL, NULL, 'Baru', '2018-12-11 03:01:37'),
(63, 112, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-12-11', NULL, NULL, 'Baru', '2018-12-11 03:03:03'),
(64, 115, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-12-11', NULL, NULL, 'Baru', '2018-12-11 03:08:25'),
(65, 116, 'kelahiran', 'Normal', 'SUSI SUSANTI', 'Rumah Bidan', NULL, '2018-12-11', NULL, NULL, 'Baru', '2018-12-11 03:11:27'),
(66, 20, 'kematian', 'sakit', 'Kuburan Muslimin', 'tidak ada', NULL, '2018-01-24', NULL, NULL, 'Perubahan', '2018-01-11 03:15:00'),
(67, 25, 'kematian', 'kecelakaan', 'Kuburan Muslimin', 'luka parah', NULL, '2018-01-01', NULL, NULL, 'Perubahan', '2018-01-11 03:15:51'),
(68, 26, 'kematian', 'sakit', 'Kuburan Muslimin', 'tidak ada', NULL, '2018-01-31', NULL, NULL, 'Perubahan', '2018-01-11 03:17:11'),
(69, 21, 'kurangmampu', '1.000.000', 'kebutuhan perekonomian tidak mencukupi', 'sederrhana', NULL, '2018-01-11', NULL, NULL, 'Perubahan', '2018-01-11 03:18:49'),
(70, 22, 'kurangmampu', '500.000', 'hanya membantu suami bekerja untuk mencukupi keper', 'sederrhana', NULL, '2018-01-11', NULL, NULL, 'Perubahan', '2018-01-11 03:19:58'),
(71, 30, 'kurangmampu', '1.000.000', 'kebutuhan ekonomi tidak mencukupi', 'sederrhana', NULL, '2018-01-11', NULL, NULL, 'Perubahan', '2018-01-11 03:21:11'),
(72, 19, 'sudahektp', 'Disdukcapil', '2018-01-11', NULL, NULL, '2018-01-25', NULL, NULL, 'Perubahan', '2018-01-11 03:22:00'),
(73, 20, 'sudahektp', 'Disdukcapil', '2018-01-02', NULL, NULL, '2018-01-09', NULL, NULL, 'Perubahan', '2018-01-11 03:22:26'),
(74, 21, 'sudahektp', 'Disdukcapil', '2018-01-11', NULL, NULL, '2018-01-23', NULL, NULL, 'Perubahan', '2018-01-11 03:22:58'),
(75, 19, 'ijinusaha', 'Perkebunan Sawit', '5 ha', '4.000.000', '2013-01-16', '2018-01-11', 1, 1, 'Perubahan', '2018-01-11 03:24:18'),
(76, 20, 'ijinusaha', 'Jualan Sembako', 'di rumah', '1.000.000', '2017-01-03', '2018-01-11', 5, 1, 'Perubahan', '2018-01-11 03:25:26'),
(77, 51, 'ijinusaha', 'Penggemukan sapi', 'di kandang sapi', '5.000.000', '2016-11-17', '2018-01-11', 3, 1, 'Perubahan', '2018-01-11 03:29:25'),
(78, 19, 'pindahdomisili', 'Keluar', 'KANDANGAN LAMA,RT : 003,Kodepos :70872			', 'Pelaihari', 'pekerjaan', '2018-01-11', 1, 2, 'Perubahan', '2018-01-11 03:31:14');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE IF NOT EXISTS `pendidikan` (
`id_pendidikan` int(11) NOT NULL,
  `nama_pendidikan` varchar(30) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id_pendidikan`, `nama_pendidikan`) VALUES
(1, 'Tidak/Belum Sekolah'),
(2, 'SD/Sederajat'),
(3, 'SLTP/Sederajat'),
(4, 'SLTA/Sederajat'),
(5, 'D3'),
(6, 'S1/D4'),
(7, 'S2'),
(8, 'S3'),
(9, 'Tidak Tamat SD/Sederajat');

-- --------------------------------------------------------

--
-- Table structure for table `penduduk`
--

CREATE TABLE IF NOT EXISTS `penduduk` (
`id_penduduk` int(11) NOT NULL,
  `id_pendidikan` int(11) DEFAULT NULL,
  `id_pekerjaan` int(11) DEFAULT NULL,
  `id_desa` int(11) NOT NULL,
  `nik` varchar(25) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `tmpt_lhr` varchar(30) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `goldar` varchar(2) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT '',
  `hub_kel` varchar(30) DEFAULT 'Anak',
  `nama_ibu` varchar(30) NOT NULL,
  `nama_ayah` varchar(30) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `master_id` int(11) NOT NULL,
  `tgl_simpan` datetime NOT NULL,
  `tgl_ubah` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

--
-- Dumping data for table `penduduk`
--

INSERT INTO `penduduk` (`id_penduduk`, `id_pendidikan`, `id_pekerjaan`, `id_desa`, `nik`, `nama`, `jk`, `tmpt_lhr`, `tgl_lhr`, `alamat`, `goldar`, `agama`, `status`, `hub_kel`, `nama_ibu`, `nama_ayah`, `keterangan`, `master_id`, `tgl_simpan`, `tgl_ubah`) VALUES
(1, 3, 1, 0, '6301061011790006', 'Fahmi', 'Laki-Laki', 'Kandangan Lama', '1979-11-10', 'halimnya', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'Bariah', 'Jamhuri', '', 0, '2018-05-12 03:11:46', '2018-05-11 14:01:23'),
(2, 2, 8, 1, '6301064106850002', 'ISNA', 'Perempuan', 'KANDANGAN LAMA', '1985-06-01', 'KANDANGAN LAMA,RT : 002,Kodepo', 'AB', 'Islam', 'Belum Kawin', 'Anak', 'ESAH', 'A. IDRUS', 'belum kawin', 33, '2018-05-11 00:37:39', '2018-05-11 14:06:43'),
(37, 3, 2, 1, '6301061808820001', 'ARDANI', 'Laki-Laki', 'Kandangan Lama', '1982-08-18', 'KANDANGAN LAMA,RT : 001,Kodepo', 'O', 'Islam', 'Kawin', 'Kepala Keluarga', 'ASMAH', 'JAHRANI', 'sudah menikah', 0, '2018-05-11 00:49:33', '2018-05-10 22:49:33'),
(15, NULL, NULL, 0, '-', 'Mariatul', 'Laki-Laki', 'palembang', '2018-05-20', 'jl.a', 'A', 'Islam', 'Belum Kawin', NULL, 'fafa', 'Fahmi', 'jalinan', 1, '2018-05-20 15:30:13', '2018-05-20 13:30:13'),
(36, 9, 9, 1, '6301062907030002', 'MUHAMMAD SAUFI', 'Laki-Laki', 'Kandangan Lama', '2003-07-29', 'KANDANGAN LAMA,RT : 002,Kodepo', 'B', 'Islam', 'Belum Kawin', 'Anak', 'ESAH', 'A. IDRUS', 'masih sekolah', 33, '2018-05-11 00:42:30', '2018-05-10 22:42:30'),
(18, NULL, NULL, 1, '6301060520180001', 'hafi', 'Laki-Laki', 'tanah laut', '2018-05-29', 'Kandangan Lama', 'B', 'Islam', 'Belum Kawin', NULL, 'Aisyah', 'Fardi', 'Susi', 17, '2018-05-29 08:22:12', '2018-05-29 06:22:12'),
(19, 6, 1, 1, '6301060908740001', 'Rani', 'Laki-Laki', 'Bati-bati', '1974-08-09', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'FAHRIAH', 'MAJENI', 'Sudah Menikah', 0, '2018-07-10 17:29:51', '2018-07-04 08:04:12'),
(20, 4, 7, 1, '6301065002830002', 'RATNAWATI', 'Perempuan', 'CINTA PURI', '1983-02-10', 'KANDANGAN LAMA,RT : 003,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'NORMAS', 'HUMIJI', 'Sudah Menikah', 19, '2018-07-10 17:33:13', '2018-07-04 08:06:03'),
(21, 3, 2, 1, '6301062106650001', 'JUM''AN', 'Laki-Laki', 'TAMBAN', '1965-06-21', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Kawin', 'Kepala Keluarga', 'ALUH ', 'ABAS', 'sudah menikah', 19, '2018-07-10 18:31:11', '2018-07-04 08:08:17'),
(22, 3, 7, 1, '6301064106760001', 'ALFISAH', 'Perempuan', 'TATAH PAMANGKIH', '1976-06-01', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'RUSMINAH', 'JOHANSYAH', 'Sudah Punya anak 2', 21, '2018-07-10 18:34:55', '2018-07-04 13:44:56'),
(23, 2, 2, 1, '6301060106790003', 'MUSTAPA', 'Laki-Laki', 'KANDANGAN LAMA', '1979-06-01', 'KANDANGAN LAMA,RT : 007,Kodepo', 'AB', 'Islam', 'Kawin', 'Kepala Keluarga', 'ALUS', 'JAINI', 'SUDAH MENIKAH', 0, '2018-07-10 18:43:27', '2018-07-05 03:47:29'),
(24, 2, 7, 1, '6301064505850001', 'MULYANI', 'Perempuan', 'Kandangan Lama', '1985-05-05', 'KANDANGAN LAMA,RT : 007,Kodepo', 'A', 'Islam', 'Kawin', 'Istri', 'RANIAH', 'BULHI', 'sudah mempunyai 2 anak', 23, '2018-07-10 18:46:13', '2018-07-05 03:49:40'),
(25, NULL, NULL, 1, '6301064106080001', 'INDAH KAROMAH', 'Perempuan', 'BARABAI', '2008-06-01', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'RATNAWATI', 'RANI', 'RAHMAWATI', 19, '2018-07-10 18:27:41', '2018-07-10 16:27:41'),
(26, NULL, NULL, 1, '6301060901020001', 'ADLIN SAPUTRA', 'Laki-Laki', 'KANDANGAN LAMA', '2002-01-09', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'ALFISAH', 'JUM''AN', 'SUSI SUSANTI', 21, '2018-07-10 18:37:17', '2018-07-10 16:37:17'),
(27, NULL, NULL, 1, '6301062505950001', 'MUHAMMAD ABRAR AMIN', 'Laki-Laki', 'KANDANGAN LAMA', '1995-05-25', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'ALFISAH', 'JUM''AN', 'TITIK P.', 21, '2018-07-10 18:39:38', '2018-07-10 16:39:38'),
(28, NULL, NULL, 1, '6301067103000001', 'NIDA AMALIAHNIDA AMALIAH', 'Perempuan', 'KANDANGAN LAMA', '2000-01-31', 'KANDANGAN LAMA,RT : 007,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'MULYANI', 'MUSTAPA', 'SUSI SUSANTI', 23, '2018-07-10 18:48:40', '2018-07-10 16:48:40'),
(29, NULL, NULL, 1, '6301065006070001', 'WIWIN', 'Perempuan', 'Tanah Laut', '2007-06-06', 'KANDANGAN LAMA,RT : 007,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'MULYANI', 'MUSTAPA', 'SUSI SUSANTI', 23, '2018-07-10 18:50:07', '2018-07-10 16:50:07'),
(30, 2, 2, 1, '6301060106420001', 'SADRI', 'Laki-Laki', 'BANJARMASIN', '1942-06-01', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Kawin', 'Kepala Keluarga', 'MARKIAH', 'SAE ', 'sudah menikah', 21, '2018-06-11 00:13:29', '2018-06-10 16:52:13'),
(31, 2, 7, 1, '6301065206610002', 'MARNIAH', 'Perempuan', 'KANDANGAN LAMA', '1961-06-12', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Kawin', 'Istri', 'AMAI', 'A. JAMAN', 'sudah menikah', 30, '2018-06-11 00:18:39', '2018-06-10 22:18:39'),
(32, 9, 2, 1, '6301064107600016', 'SAPIAH', 'Perempuan', 'KANDANGAN LAMA', '1960-07-01', 'KANDANGAN LAMA,RT : 006,Kodepo', 'O', 'Islam', 'Janda', 'Kepala Keluarga', 'BADARIAH', 'IDUP', 'janda sudah lama', 0, '2018-06-11 00:22:04', '2018-06-10 22:22:04'),
(33, 2, 2, 1, '6301060106620002', 'A. IDRUS', 'Laki-Laki', 'KANDANGAN LAMA', '1962-06-01', 'KANDANGAN LAMA,RT : 002,Kodepo', 'AB', 'Islam', 'Duda', 'Kepala Keluarga', 'PUNIATI ', 'SAKRANI', 'sudah menikah', 0, '2018-06-11 00:30:58', '2018-06-10 22:27:08'),
(34, 3, 8, 1, '6301061003920002', 'RAHMI', 'Laki-Laki', 'KANDANGAN LAMA', '1992-03-10', 'KANDANGAN LAMA,RT : 002,Kodepo', 'AB', 'Islam', 'Belum Kawin', 'Anak', 'ESAH', 'A. IDRUS', 'anak pertama', 33, '2018-06-11 00:34:35', '2018-06-10 22:29:57'),
(35, 4, 9, 1, '6301061701930002', 'RUSMININ', 'Laki-Laki', 'KANDANGAN LAMA', '1993-01-17', 'KANDANGAN LAMA,RT : 002,Kodepo', 'B', 'Islam', 'Belum Kawin', 'Anak', 'ESAH', 'A. IDRUS', 'belum kawin', 33, '2018-06-11 00:34:20', '2018-06-10 22:34:20'),
(38, 3, 7, 1, '6301065905910003', 'RINI RUSYIDA', 'Laki-Laki', 'Kandangan Lama', '1991-05-19', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'FARIDAH', 'ARBAIDIN', 'sudah menikah', 37, '2018-07-11 01:23:09', '2018-07-10 23:23:09'),
(39, NULL, NULL, 1, '6301061412110002', 'AHMAD SAUFI', 'Laki-Laki', 'TANAH LAUT', '2011-04-11', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'RINI RUSYIDA', 'ARDANI', 'SUSI SUSANTI', 37, '2018-05-11 01:31:10', '2018-05-10 23:31:10'),
(40, 3, 2, 1, '6301060910810002', 'HAIRUNI', 'Laki-Laki', 'KANDANGAN LAMA', '1981-10-09', 'KANDANGAN LAMA,RT : 006,Kodepo', 'O', 'Islam', 'Kawin', 'Kepala Keluarga', 'MASTIAH', 'SULAIMAN', 'sudah menikah', 0, '2018-05-11 01:34:35', '2018-05-10 23:34:35'),
(41, 9, 9, 1, '6301064101850003', 'ASPIATI', 'Perempuan', 'BATAKAN', '1985-01-01', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'RUMBAYYAH', 'KADERUN', 'sudah menikah', 40, '2018-05-11 01:39:25', '2018-05-10 23:39:25'),
(42, NULL, NULL, 1, '6301060611050001', 'AHMAD AJAZI', 'Laki-Laki', 'BATAKAN', '2005-11-06', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'ASPIATI', 'HAIRUNI', 'Eka Safitri', 40, '2018-05-11 01:41:39', '2018-05-10 23:41:39'),
(43, NULL, NULL, 1, '6301062205140001', 'MUHAMMAD ROFI`I', 'Laki-Laki', 'TANAH LAUT', '2014-05-25', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'ASPIATI', 'HAIRUNI', 'Eka Safitri', 40, '2018-05-11 01:43:31', '2018-05-10 23:43:31'),
(44, 1, 2, 1, '6301061110570001', 'SULAIMAN', 'Laki-Laki', 'KANDANGAN LAMA', '1957-10-11', 'KANDANGAN LAMA,RT : 006,Kodepo', 'AB', 'Islam', 'Kawin', 'Kepala Keluarga', 'SALAMAH', 'SALIM', 'sudah menikah', 0, '2018-04-11 01:46:34', '2018-04-10 23:46:34'),
(45, 4, 8, 1, '6301066111600001', 'JAIYAH', 'Perempuan', 'Kandangan Lama', '1960-11-21', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'BAHRAH', 'NAFIS', 'sudah menikah', 44, '2018-04-11 01:57:02', '2018-04-10 23:57:02'),
(46, NULL, NULL, 1, '6301065409870001', 'AIMAH', 'Perempuan', 'Kandangan Lama', '1987-09-14', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'JAIYAH', 'SULAIMAN', 'TITIK P.', 44, '2018-04-11 01:58:53', '2018-04-10 23:58:53'),
(47, 2, 2, 1, '6301062106610001', 'JAMRAH', 'Laki-Laki', 'KANDANGAN LAMA', '1961-06-21', 'KANDANGAN LAMA,RT : 007,Kodepo', 'O', 'Islam', 'Kawin', 'Kepala Keluarga', 'GALUH', 'RABAN', 'sudah menikah', 0, '2018-04-11 02:01:47', '2018-04-11 00:01:47'),
(48, 2, 7, 1, '6301065808700003', 'SA IYAH', 'Perempuan', 'Kandangan Lama', '1970-08-18', 'KANDANGAN LAMA,RT : 007,Kodepo', 'AB', 'Islam', 'Kawin', 'Istri', 'JA''IYAH', 'PARHAN', 'sudah menikah', 47, '2018-04-11 02:13:37', '2018-04-11 00:13:37'),
(49, NULL, NULL, 1, '6301062602920001', 'AHMAD SUPIANI', 'Laki-Laki', 'Kandangan Lama', '1992-02-26', 'KANDANGAN LAMA,RT : 007,Kodepo', 'O', 'Islam', 'Belum Kawin', NULL, 'SA IYAH', 'JAMRAH', 'SUSI SUSANTI', 47, '2018-04-11 02:15:29', '2018-04-11 00:15:29'),
(50, NULL, NULL, 1, '6301065501950001', 'RUHITA', 'Perempuan', 'Kandangan Lama', '1995-01-15', 'KANDANGAN LAMA,RT : 007,Kodepo', 'AB', 'Islam', 'Belum Kawin', NULL, 'SA IYAH', 'JAMRAH', 'SUSI SUSANTI', 47, '2018-04-11 02:17:57', '2018-04-11 00:17:57'),
(51, 2, 2, 1, '6301060510720003', 'PARANSYAH', 'Laki-Laki', 'Kandangan Lama', '1972-10-05', 'KANDANGAN LAMA,RT : 001,Kodepo', '', 'Islam', 'Kawin', 'Kepala Keluarga', 'ASIAH', 'IPRANI', 'sudah menikah', 0, '2018-03-11 01:20:48', '2018-03-11 00:20:48'),
(52, 2, 7, 1, '6301064805750004', 'SARYATI', 'Perempuan', 'Kandangan Lama', '1975-05-08', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'SARINAH', 'UMRAH', 'sudah menikah', 51, '2018-03-11 01:22:39', '2018-03-11 00:22:39'),
(53, NULL, NULL, 1, '6301060307050002', 'SUPRIYADI', 'Laki-Laki', 'TANAH LAUT', '2005-07-03', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'SARYATI', 'PARANSYAH', 'SUSI SUSANTI', 51, '2018-03-11 01:24:06', '2018-03-11 00:24:06'),
(54, 1, 2, 1, '6301061207610002', 'HARSANI', 'Laki-Laki', 'Kandangan Lama', '1961-07-12', 'KANDANGAN LAMA,RT : 001,Kodepo', '', 'Islam', 'Kawin', 'Kepala Keluarga', 'NORBAYAH', 'JAMAN', 'sudah menikah', 0, '2018-03-11 01:27:21', '2018-03-11 00:27:21'),
(55, 1, 7, 1, '6301064708630002', 'BAINAH', 'Perempuan', 'Kandangan Lama', '1963-08-07', 'KANDANGAN LAMA,RT : 001,Kodepo', 'AB', 'Islam', 'Kawin', 'Istri', 'PURNAMA', 'TONA', 'sudah menikah', 54, '2018-03-11 01:29:43', '2018-03-11 00:29:43'),
(56, NULL, NULL, 1, '6301065405990001', 'ERIKA YULIANA', 'Perempuan', 'Kandangan Lama', '1995-05-14', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'BAINAH', 'HARSANI', 'SUSI SUSANTI', 54, '2018-03-11 01:31:09', '2018-03-11 00:31:09'),
(57, 2, 2, 1, '6301061304630001', 'BURHANI', 'Laki-Laki', 'BATU TUNGKU', '1963-04-13', 'KANDANGAN LAMA,RT : 008,Kodepo', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'JAURAH', 'SYAWAL', 'sudah menikah', 0, '2018-03-11 01:33:01', '2018-03-11 00:33:01'),
(58, 2, 7, 1, '6301064609700001', 'MUHLISAH', 'Perempuan', 'AMPARAYA', '1970-09-06', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'ZAHRAH', 'MAJERI', 'sudah menikah', 57, '2018-03-11 01:34:33', '2018-03-11 00:34:33'),
(59, NULL, NULL, 1, '6301061706920001', 'ALI FAHMI', 'Laki-Laki', 'Kandangan Lama', '1992-06-17', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'MUHLISAH', 'BURHANI', 'SUSI SUSANTI', 57, '2018-03-11 01:35:58', '2018-03-11 00:35:58'),
(60, NULL, NULL, 1, '6301064607940002', 'SRI WAHYUNI', 'Perempuan', 'BATU TUNGKU', '1994-07-06', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'MUHLISAH', 'BURHANI', 'SUSI SUSANTI', 57, '2018-02-11 01:40:50', '2018-02-11 00:40:50'),
(61, NULL, NULL, 1, '6301065506950002', 'LISA FITRIYANI', 'Perempuan', 'KANDANGAN LAMA', '1995-06-16', 'KANDANGAN LAMA,RT : 008,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'MUHLISAH', 'BURHANI', 'SUSI SUSANTI', 57, '2018-02-11 01:41:54', '2018-02-11 00:41:54'),
(62, NULL, NULL, 1, '6301061110000001', 'SAHRIL RAMADANI', 'Laki-Laki', 'Kandangan Lama', '2000-10-11', 'KANDANGAN LAMA,RT : 008,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'MUHLISAH', 'BURHANI', 'SUSI SUSANTI', 57, '2018-02-11 01:43:42', '2018-02-11 00:43:42'),
(63, 4, 2, 1, '6301060107850066', 'H.HERIANSYAH', 'Laki-Laki', 'KANDANGAN LAMA', '1975-07-10', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Kawin', 'Kepala Keluarga', 'FATIMAH', 'MADI', 'sudah menikah', 0, '2018-02-11 01:45:49', '2018-02-11 00:45:49'),
(64, 3, 7, 1, '6301064609940003', 'ISMANIAH', 'Perempuan', 'TANAH LAUT', '1994-09-06', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Kawin', 'Istri', 'RAIMAH', 'SALMANI', 'sudah menikah', 63, '2018-02-11 01:48:20', '2018-02-11 00:48:20'),
(65, NULL, NULL, 1, '6301062604120001', 'ZAIRULLAH', 'Laki-Laki', 'TANAH LAUT', '2016-12-24', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'MARIA ULFAH', 'H.HERIANSYAH', 'SUSI SUSANTI', 63, '2018-01-11 01:50:49', '2018-01-11 00:50:49'),
(66, NULL, NULL, 1, '6301060105150001', 'M.RAUHAN FIKRI', 'Laki-Laki', 'TANAH LAUT', '2015-05-01', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'ISMANIAH', 'H.HERIANSYAH', 'SUSI SUSANTI', 63, '2018-01-11 01:53:10', '2018-01-11 00:53:10'),
(67, 2, 2, 1, '6301062403860002', 'HAKIM FADILLAH', 'Laki-Laki', 'KANDANGAN LAMA', '1986-03-24', 'KANDANGAN LAMA,RT : 008,Kodepo', 'O', 'Islam', 'Kawin', 'Kepala Keluarga', 'MAHANI', 'ASRANI', 'sudah menikah', 0, '2018-01-11 01:55:12', '2018-01-11 00:55:12'),
(68, 3, 7, 1, '6301064503900006', 'KHAIRIAH', 'Perempuan', 'KANDANGAN LAMA', '1990-03-05', 'KANDANGAN LAMA,RT : 008,Kodepo', 'O', 'Islam', 'Kawin', 'Istri', 'DARIAH', 'ERMAH', 'sudah menikah', 67, '2018-01-11 01:56:56', '2018-01-11 00:56:56'),
(69, NULL, NULL, 1, '6301060902120001', 'HAIRULLAH', 'Laki-Laki', 'TANAH LAUT', '2012-02-09', 'KANDANGAN LAMA,RT : 008,Kodepo', 'O', 'Islam', 'Belum Kawin', NULL, 'KHAIRIAH', 'HAKIM FADILLAH', 'SUSI SUSANTI', 67, '2018-01-11 01:58:48', '2018-01-11 00:58:48'),
(70, 2, 2, 1, '6301060201810004', 'HADIR', 'Laki-Laki', 'KURINGKIT', '1981-01-02', 'KANDANGAN LAMA,RT : 001,Kodepo', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'SA''DIAH', 'HULDANI', 'sudah menikah', 0, '2018-08-11 03:01:22', '2018-08-11 01:01:22'),
(71, 2, 7, 1, '6301064807820001', 'ERNAWATI', 'Perempuan', 'Kandangan Lama', '1982-07-08', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'SITI UMRAH', 'SARKAWI', 'sudah menikah', 70, '2018-08-11 03:03:09', '2018-08-11 01:03:09'),
(72, NULL, NULL, 1, '6301065308120001', 'RAMADHONIYAH', 'Perempuan', 'TANAH LAUT', '2012-08-13', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'ERNAWATI', 'HADIR', 'SUSI SUSANTI', 70, '2018-08-11 03:04:44', '2018-08-11 01:04:44'),
(73, 4, 2, 1, '6301060805900001', 'MUCHAMMAD NOR', 'Laki-Laki', 'MARTAPURA', '1990-05-08', 'KANDANGAN LAMA,RT : 004', 'AB', 'Islam', 'Kawin', 'Kepala Keluarga', 'HJ.LISTARI', 'H.SUTRISNO', 'sudah menikah', 0, '2018-08-11 03:06:39', '2018-08-11 01:06:39'),
(74, 4, 7, 1, '6301066010920003', 'KHAIRIAH', 'Perempuan', 'KANDANGAN LAMA', '1992-10-20', 'KANDANGAN LAMA,RT : 004', 'B', 'Islam', 'Kawin', 'Istri', 'JAINAH', 'M.YUSUF', 'sudah menikah', 73, '2018-08-11 03:08:31', '2018-08-11 01:08:31'),
(75, NULL, NULL, 1, '6301062308130001', 'MUHAMMAD RIFKAN FAQIH', 'Laki-Laki', 'Tanah Laut', '2013-08-23', 'KANDANGAN LAMA,RT : 004', 'A', 'Islam', 'Belum Kawin', NULL, 'KHAIRIAH', 'MUCHAMMAD NOR', 'SUSI SUSANTI', 73, '2018-08-11 03:09:59', '2018-08-11 01:09:59'),
(76, 2, 2, 1, '6301060602820001', 'SARHANI', 'Laki-Laki', 'KANDANGAN LAMA', '1982-02-06', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'NIYAH', 'BUSRAH', 'sudah menikah', 0, '2018-08-11 03:12:00', '2018-08-11 01:12:00'),
(77, 2, 7, 1, '6301064107850048', 'AISYAH', 'Perempuan', 'BATAKAN', '1975-07-01', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'SAWIAH', 'AINI', 'sudah menikah', 76, '2018-08-11 03:13:36', '2018-08-11 01:13:36'),
(78, NULL, NULL, 1, '6301061312060002', 'MUHAMMAD ALKHALIFI', 'Laki-Laki', 'TANAH LAUT', '2006-12-13', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'AISYAH', 'SARHANI', 'SUSI SUSANTI', 76, '2018-08-11 03:15:28', '2018-08-11 01:15:28'),
(79, NULL, NULL, 1, '6301061705120001', 'MUHAMMAD ASFIA HAZNI', 'Laki-Laki', 'TANAH LAUT', '2012-05-17', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'AISYAH', 'SARHANI', 'SUSI SUSANTI', 76, '2018-08-11 03:16:51', '2018-08-11 01:16:51'),
(80, 2, 2, 1, '6301062507820004', 'RAHMAN', 'Laki-Laki', 'BATAKAN', '1982-07-25', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Kawin', 'Kepala Keluarga', 'NORBAYAH', 'MUHAMMAD', 'sudah menikah', 0, '2018-09-11 03:23:10', '2018-09-11 01:23:10'),
(81, 2, 7, 1, '6301064106870002', 'ISNA', 'Perempuan', 'Kandangan Lama', '1987-06-01', 'KANDANGAN LAMA,RT : 001,Kodepo', 'A', 'Islam', 'Kawin', 'Istri', 'ESAH', 'A.IDRUS', 'sudah menikah', 80, '2018-09-11 03:24:52', '2018-09-11 01:24:52'),
(82, NULL, NULL, 1, '6301061309070006', 'M.KHAIRUL UMAM', 'Laki-Laki', 'TANAH LAUT', '2007-09-13', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'ISNA', 'RAHMAN', 'SUSI SUSANTI', 80, '2018-09-11 03:26:13', '2018-09-11 01:26:13'),
(83, 4, 2, 1, '6301061503780001', 'MURDIANSYAH', 'Laki-Laki', 'KANDANGAN LAMA', '1978-03-15', 'KANDANGAN LAMA,RT : 007,Kodepo', 'O', 'Islam', 'Duda', 'Kepala Keluarga', 'RUSNAH', 'HUSIN ', 'duda', 0, '2018-09-11 03:28:40', '2018-09-11 01:28:40'),
(84, 2, 7, 1, '6301066912450001', 'SITI HALIMAH', 'Perempuan', 'KANDANGAN LAMA', '1945-12-29', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Janda', 'Kepala Keluarga', 'FATIMAH', 'ANANG GANAL', 'janda', 0, '2018-09-11 03:30:30', '2018-09-11 01:30:30'),
(85, 4, 2, 1, '6301061408830004', 'ANTUNG SURYANADI', 'Laki-Laki', 'BARABAI', '1983-08-14', 'KANDANGAN LAMA,RT : 001,Kodepo', 'AB', 'Islam', 'Kawin', 'Kepala Keluarga', 'NUR JAYANTI', 'ANANG HASAN', 'sudah menikah', 0, '2018-09-11 03:35:09', '2018-09-11 01:35:09'),
(86, 4, 7, 1, '6301064112870004', 'IDAYANTI', 'Perempuan', 'KANDANGAN LAMA', '1987-01-12', 'KANDANGAN LAMA,RT : 001,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'ARBAYAH', 'AHMANI', 'sudah menikah', 85, '2018-09-11 03:36:55', '2018-09-11 01:36:55'),
(87, NULL, NULL, 1, '6301064711120001', 'AULIA RAHMAH', 'Perempuan', 'TANAH LAUT', '2012-11-07', 'KANDANGAN LAMA,RT : 001,Kodepo', 'AB', 'Islam', 'Belum Kawin', NULL, 'IDAYANTI', 'ANTUNG SURYANADI', 'SUSI SUSANTI', 85, '2018-09-11 03:38:20', '2018-09-11 01:38:20'),
(88, 4, 6, 1, '6301061605840001', 'LAMHOT SARIANTO BERUTU', 'Laki-Laki', 'MEDAN', '1984-05-16', 'KANDANGAN LAMA,RT : 008,Kodepo', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'MINAH BANCIN', 'JALINER BERUTU', 'sudah menikah', 0, '2018-09-11 03:40:02', '2018-09-11 01:40:02'),
(89, 3, 7, 1, '6301066504820002', 'YUTI HARTATI', 'Perempuan', 'TANJUNG DEWA', '1982-04-25', 'KANDANGAN LAMA,RT : 008,Kodepo', 'A', 'Islam', 'Kawin', 'Istri', 'ARBAYAH', 'SARHANI', 'sudah menikah', 88, '2018-09-11 03:42:49', '2018-09-11 01:42:49'),
(90, NULL, NULL, 1, '6301062012100003', 'MUHAMMAD IRSYAD CHYRISITIAN BE', 'Laki-Laki', 'TANAH LAUT', '2010-12-20', 'KANDANGAN LAMA,RT : 008,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'YUTI HARTATI', 'LAMHOT SARIANTO BERUTU', 'SUSI SUSANTI', 88, '2018-09-11 03:44:51', '2018-09-11 01:44:51'),
(91, 2, 2, 1, '6301060107540012', 'MUHAMMAD YUSUF', 'Laki-Laki', 'KANDANGAN LAMA', '1954-07-01', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'ATU', 'ARAH', 'sudah menikah', 0, '2018-10-11 04:15:33', '2018-10-11 02:15:33'),
(92, 2, 7, 1, '6301064205610002', 'MAHMUDAH', 'Perempuan', 'KANDANGAN LAMA', '1961-05-02', 'KANDANGAN LAMA,RT : 003,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'BADARIAH', 'BASRI', 'sudah menikah', 91, '2018-10-11 04:16:58', '2018-10-11 02:16:58'),
(93, NULL, NULL, 1, '6301060107990018', 'RIZA AZHARI', 'Laki-Laki', 'TANAH LAUT', '1999-07-01', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'MAHMUDAH', 'MUHAMMAD YUSUF', 'SUSI SUSANTI', 91, '2018-10-11 04:18:26', '2018-10-11 02:18:26'),
(94, NULL, NULL, 1, '6301060602920001', 'ARBAIN', 'Laki-Laki', 'KANDANGAN LAMA', '1992-02-06', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'MAHMUDAH', 'MUHAMMAD YUSUF', 'SUSI SUSANTI', 91, '2018-10-11 04:19:31', '2018-10-11 02:19:31'),
(95, 3, 4, 1, '6301060606740001', 'MASHADIANSYAH', 'Laki-Laki', 'KANDANGAN LAMA', '1974-06-06', 'DESA KANDANGAN LAMA,RT : 005,K', 'AB', 'Islam', 'Kawin', 'Kepala Keluarga', 'MASTINAH', 'M. SYAHRANI', 'sudah menikah', 0, '2018-10-11 04:21:03', '2018-10-11 02:21:03'),
(96, 4, 7, 1, '6301064606750001', 'KUSTINAH', 'Perempuan', 'KANDANGAN LAMA', '1975-06-06', 'DESA KANDANGAN LAMA,RT : 005,K', 'A', 'Islam', 'Kawin', 'Istri', 'MASRIAH', 'FITANI', 'sudah menikah', 95, '2018-10-11 04:22:31', '2018-10-11 02:22:31'),
(97, NULL, NULL, 1, '6301061207000002', 'AHMAD ROFI`I', 'Laki-Laki', 'TANAH LAUT', '2000-07-12', 'DESA KANDANGAN LAMA,RT : 005,K', 'B', 'Islam', 'Belum Kawin', NULL, 'KUSTINAH', 'MASHADIANSYAH', 'SUSI SUSANTI', 95, '2018-10-11 04:24:21', '2018-10-11 02:24:21'),
(98, NULL, NULL, 1, '6301064903960001', 'HUSNA HIDAYATIE', 'Perempuan', 'KANDANGAN LAMA', '1996-03-09', 'DESA KANDANGAN LAMA,RT : 005,K', 'A', 'Islam', 'Belum Kawin', NULL, 'KUSTINAH', 'MASHADIANSYAH', 'SUSI SUSANTI', 95, '2018-10-11 04:25:34', '2018-10-11 02:25:34'),
(99, 3, 2, 1, '6301062705870001', 'AMIN', 'Laki-Laki', 'ALUH-ALUH KECIL', '1987-05-27', 'KANDANGAN LAMA,RT : 006,Kodepo', 'B', 'Islam', 'Kawin', 'Kepala Keluarga', 'SITI PATIMAH ', 'MAWAN', 'sudah menikah', 0, '2018-11-11 03:28:28', '2018-11-11 02:28:28'),
(100, 3, 1, 1, '6301064708730001', 'LIMATU', 'Perempuan', 'KANDANGAN LAMA', '1973-08-07', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Kawin', 'Istri', 'RAHMAH', 'SAILI', 'sudah menikah', 99, '2018-11-11 03:30:15', '2018-11-11 02:30:15'),
(101, NULL, NULL, 1, '6301066107060001', 'LAILATUL ZULAIKHA', 'Perempuan', 'TANAH LAUT', '2006-07-21', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'LIMATU', 'AMIN', 'SUSI SUSANTI', 99, '2018-11-11 03:32:29', '2018-11-11 02:32:29'),
(102, NULL, NULL, 1, '6301061005110001', 'MUHAMMAD NABILLA AKBAR', 'Laki-Laki', 'TANAH LAUT', '2011-05-10', 'KANDANGAN LAMA,RT : 006,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'LIMATU ', 'AMIN', 'SUSI SUSANTI', 99, '2018-11-11 03:33:59', '2018-11-11 02:33:59'),
(103, 2, 2, 1, '6301062005880004', 'MASDAR', 'Laki-Laki', 'BATAKAN', '1988-05-20', 'KANDANGAN LAMA,RT :,Kodepos :7', 'AB', 'Islam', 'Kawin', 'Kepala Keluarga', 'JANNAH', 'MUSTAPA', 'sudah menikah', 0, '2018-11-11 03:38:00', '2018-11-11 02:38:00'),
(104, 2, 7, 1, '6301064712940004', 'SA''ADAH', 'Perempuan', 'KANDANGAN LAMA', '1994-12-07', 'KANDANGAN LAMA,RT :,Kodepos :7', 'B', 'Islam', 'Kawin', 'Istri', 'SAMSIAH', 'AMAT AINI', 'sudah menikah', 103, '2018-11-11 03:39:27', '2018-11-11 02:39:27'),
(105, NULL, NULL, 1, '6301064408110004', 'SULIS', 'Perempuan', 'KANDANGAN LAMA', '2011-08-04', 'KANDANGAN LAMA,RT :,Kodepos :7', 'A', 'Islam', 'Belum Kawin', NULL, 'SA''ADAH', 'MASDAR', 'SUSI SUSANTI', 103, '2018-11-11 03:40:58', '2018-11-11 02:40:58'),
(106, 2, 2, 1, '6301062004910001', 'MUHAMMAD', 'Laki-Laki', 'KANDANGAN LAMA', '1991-04-20', 'KANDANGAN LAMA,RT : 004,Kodepo', 'AB', 'Islam', 'Kawin', 'Kepala Keluarga', 'MARFU''AH', 'BUSTANI', 'sudah menikah', 0, '2018-11-11 03:42:36', '2018-11-11 02:42:36'),
(107, 2, 7, 1, '6301065608940004', 'RUSMILAWATI', 'Perempuan', 'BARABAI', '1994-08-16', 'KANDANGAN LAMA,RT : 004,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'SALASIAH', 'RUSTAM', 'sudah menikah', 106, '2018-11-11 03:44:09', '2018-11-11 02:44:09'),
(108, NULL, NULL, 1, '6301062901120002', 'MUHAMMAD BASSAM NAZIH', 'Laki-Laki', 'TANAH LAUT', '2012-01-29', 'KANDANGAN LAMA,RT : 004,Kodepo', 'AB', 'Islam', 'Belum Kawin', NULL, 'RUSMILAWATI', 'MUHAMMAD', 'SUSI SUSANTI', 106, '2018-11-11 03:46:01', '2018-11-11 02:46:01'),
(109, 3, 10, 1, '6301060711830004', 'SURYAWAN', 'Laki-Laki', 'KANDANGAN LAMA', '1983-11-07', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Kawin', 'Kepala Keluarga', 'TARMAS', 'TUMAR', 'sudah menikah', 0, '2018-12-11 03:58:26', '2018-12-11 02:58:26'),
(110, 3, 7, 1, '6301065410890002', 'HJ. AIDA', 'Perempuan', 'KANDANGAN LAMA', '1989-10-14', 'KANDANGAN LAMA,RT : 003,Kodepo', 'B', 'Islam', 'Kawin', 'Istri', 'JATIAH', 'SARWANI', 'sudah menikah', 109, '2018-12-11 04:00:02', '2018-12-11 03:00:02'),
(111, NULL, NULL, 1, '6301061411050002', 'DIAN AGRIANDA', 'Laki-Laki', 'TANAH LAUT', '2005-11-14', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'HJ. AIDA', 'SURYAWAN', 'SUSI SUSANTI', 109, '2018-12-11 04:01:37', '2018-12-11 03:01:37'),
(112, NULL, NULL, 1, '6301061709130001', 'MUHAMMAD AZRIEL', 'Laki-Laki', 'TANAH LAUT', '2009-09-17', 'KANDANGAN LAMA,RT : 003,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'HJ. AIDA', 'SURYAWAN', 'SUSI SUSANTI', 109, '2018-12-11 04:03:03', '2018-12-11 03:03:03'),
(113, 3, 2, 1, '6301061106800004', 'MAHLIANSYAH', 'Laki-Laki', 'Kandangan Lama', '1980-06-11', 'KANDANGAN LAMA,RT : 002,Kodepo', 'B', 'Islam', 'Kawin', 'Kepala Keluarga', 'SALMAH', 'ABDURRAHMAN', 'sudah menikah', 0, '2018-12-11 04:04:37', '2018-12-11 03:04:37'),
(114, 3, 7, 1, '6301064807870004', 'DINA MARIANA', 'Perempuan', 'Kandangan Lama', '1987-07-08', 'KANDANGAN LAMA,RT : 002,Kodepo', 'A', 'Islam', 'Kawin', 'Istri', 'FATIMAH', 'MUHAMMAD YAMIN', 'sudah menikah', 113, '2018-12-11 04:06:28', '2018-12-11 03:06:28'),
(115, NULL, NULL, 1, '6301066602100001', 'JIADATUL AMELIA', 'Perempuan', 'TANAH LAUT', '2010-02-06', 'KANDANGAN LAMA,RT : 002,Kodepo', 'B', 'Islam', 'Belum Kawin', NULL, 'DINA MARIANA', 'MAHLIANSYAH', 'SUSI SUSANTI', 113, '2018-12-11 04:08:25', '2018-12-11 03:08:25'),
(116, NULL, NULL, 1, '6301066401140002', 'JANNATUL AZWA', 'Perempuan', 'TANAH LAUT', '2014-01-24', 'KANDANGAN LAMA,RT : 002,Kodepo', 'A', 'Islam', 'Belum Kawin', NULL, 'DINA MARIANA', 'MAHLIANSYAH', 'SUSI SUSANTI', 113, '2018-12-11 04:11:27', '2018-12-11 03:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
`id_pengguna` int(11) NOT NULL,
  `nama_pengguna` varchar(30) NOT NULL,
  `kata_sandi` varchar(20) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `level` enum('Administrator','Petugas') NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `nama_pengguna`, `kata_sandi`, `nama_lengkap`, `level`) VALUES
(2, 'admin', 'admin', 'Administrator', 'Administrator'),
(3, 'mirna', 'mirna', 'MIRNA HIDAYATI', 'Petugas');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE IF NOT EXISTS `provinsi` (
`id_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(30) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `nama_provinsi`) VALUES
(1, 'Kalimantan Selatan');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keluar`
--

CREATE TABLE IF NOT EXISTS `surat_keluar` (
`id_surat_masuk` int(11) NOT NULL,
  `no_surat` varchar(20) NOT NULL,
  `tujuan` varchar(30) NOT NULL,
  `perihal` varchar(30) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `sifat` varchar(10) NOT NULL,
  `tanggal_surat` date NOT NULL,
  `no_agenda` varchar(6) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bidang_usaha`
--
ALTER TABLE `bidang_usaha`
 ADD PRIMARY KEY (`id_bidang_usaha`);

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
 ADD PRIMARY KEY (`id_desa`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
 ADD PRIMARY KEY (`id_kabupaten`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
 ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
 ADD PRIMARY KEY (`id_pekerjaan`);

--
-- Indexes for table `pendataan`
--
ALTER TABLE `pendataan`
 ADD PRIMARY KEY (`id_pendataan`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
 ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `penduduk`
--
ALTER TABLE `penduduk`
 ADD PRIMARY KEY (`id_penduduk`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
 ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
 ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
 ADD PRIMARY KEY (`id_surat_masuk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bidang_usaha`
--
ALTER TABLE `bidang_usaha`
MODIFY `id_bidang_usaha` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `desa`
--
ALTER TABLE `desa`
MODIFY `id_desa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
MODIFY `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
MODIFY `id_pekerjaan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pendataan`
--
ALTER TABLE `pendataan`
MODIFY `id_pendataan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `penduduk`
--
ALTER TABLE `penduduk`
MODIFY `id_penduduk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
MODIFY `id_provinsi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
MODIFY `id_surat_masuk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
