
<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		<?=$title?></h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
	<?php
		if(isset($_GET['ubah'])){
			$id=$this->input->get('id');
			$this->db->where(['a.id_penduduk'=>$id]);
			$get=$this->pendudukModel->get_data()->row_array();
			extract($get);
			$parameter='ubah';
		}
		else{
			$parameter='simpan';
		}
	?>

	<form class="validate form-horizontal" id="form-penduduk" method="POST" action="<?=site_url()?>penduduk/<?=$parameter?>/pendataan">
				<?=input_hidden('id_penduduk',(isset($id_penduduk)?$id_penduduk:''),'md-input','required');?>	
				<?=input_hidden('parameter',(isset($parameter)?$param['param']:''),'md-input','required');?>	
                
                            <ul  class="nav nav-pills">
                                <li  class="active">
                                    <a href="#2a" data-toggle="tab">Perubahan Status</a>
                                </li>
                            </ul>
                            <div class="tab-content clearfix">
                            <div class="tab-pane" id="1a">
                				<div class="row form-penduduk">
					            <?php //include('include/formpenduduk.php')?>
                                </div>
                            </div>
                                                    
                            <?php
                                if(isset($_GET['ubah'])){
                                    $id=$this->input->get('id');
                                    $this->db->where(['e.id_pendataan'=>$id]);
                                    $get=$this->pendudukModel->get_dataParam()->row_array();
                                    extract($get);
                                    $parameter='ubah';
                                }
                                else{
                                    $parameter='simpan';
                                }
                            ?>
				            <?=input_hidden('id_pendataan',(isset($id_pendataan)?$id_pendataan:''),'md-input','required');?>		                
                            <?=input_hidden('status_pendataan',(isset($status_pendataan)?$status_pendataan:'Perubahan'),'md-input','required');?>	
                            
                            <div class="tab-pane active" id="2a">
                                <div class="row form-status">
                                    
                                    <div class="col-md-9 load-penduduk">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>NIK/Nama Penduduk</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?php 
                                                $op=NULL;
                                                $op['']='Pilih Salah Satu';  
                                                $this->db->order_by('nama','ASC');
                                                $data=$this->db->get('penduduk');
                                                foreach($data->result() as $row){
                                                    $op[$row->id_penduduk]=$row->nama;
                                                }
                                                echo select('id_penduduk',$op,(isset($id_penduduk)?$id_penduduk:''),'','');?>
                                            </div>
                                        </div>			
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>Status</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?php 
                                                $op=NULL;
                                                $op['']='Pilih Salah Satu';  
                                                $op['Kawin']='Kawin';
                                                $op['Belum Kawin']='Belum Kawin';
                                                $op['Janda']='Janda';
                                                $op['Duda']='Duda';
                                                echo select('pendataan',$op,(isset($pendataan)?$pendataan:''),'','');?>
                                            </div>
                                        </div>			
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>Keterangan</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?=textarea('keterangan',(isset($keterangan)?$keterangan:''));?>
                                            </div>
                                        </div>			
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>Tanggal</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?=input_date('tanggal',(isset($tanggal)?$tanggal:date('Y-m-d')));?>
                                            </div>
                                        </div>			
                                    </div>
                                </div>
                                </div>
						<div class="clearfix"></div>
						</div>
                        <div class="col-md-12" style="background: #fff;border: 1px solid #eee;padding-top: 10px;">
                            <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                            <a href="<?=site_url('penduduk/index/'.$param['param'])?>" class="btn btn-danger">Kembali</a>
                        </div>
		</form>
    </div>
</div>
<?php } else { ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		<?=$title?></h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">

					<!-- table -->
					<a href="<?=site_url('penduduk/index/status?tambah')?>" class="btn btn-success btn-tambah">
						<i class='fa fa-plus'></i> Tambah
					</a>
					<hr>
                    <form>
                        <?php
                            $bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
                            $tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
                        ?>
                        <div class="row">
                            <div class="col-md-4">
                                <?=select('bulan',bulanList(),$bulan)?>
                            </div>
                            <div class="col-md-4">
                                <?=select('tahun',tahunList(),$tahun)?>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-info">Lihat</button>
                                <a href="<?=site_url('cetak')?>?jenis=status&bulan=<?=$bulan?>&tahun=<?=$tahun?>" class="btn btn-warning" target="_blank">Cetak</a>
                            </div>
                        </div>
                    </form>
					<?php echo $this->session->flashdata('info');?>
					<!-- end table -->
					<?php
					$template = array(
                            'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
                        );
                        $this->table->set_template($template);
                        $this->table->set_heading('No','NIK','Nama','Alamat','Status','Tanggal','Keterangan','');
                        $this->db->where($param['where']);
                        if($bulan!=''){
                            $this->db->where("DATE_FORMAT(tanggal,'%m')='".$bulan."'");
                        }
                        if($tahun!=''){
                            $this->db->where("DATE_FORMAT(tanggal,'%Y')='".$tahun."'");
                        }
                        $get_data=$this->pendudukModel->get_dataParamStatus();
                        $i=1;
                        foreach($get_data->result() as $row){
                            $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                                        $row->nik,
                                                                        $row->nama,
                                                                        $row->alamat,
                                                                        $row->pendataan,
                                                                        standar_tanggal($row->tanggal),
                                                                        $row->keterangan,
                                                                        array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                                        <li>'.anchor(site_url("penduduk/index/status?ubah&id=".$row->id_penduduk),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                                        <li>'.anchor(site_url("penduduk/hapus?id_penduduk=".$row->id_penduduk),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                                        </ul>',"width"=>"20px","align"=>"center"));
                            $i++;
                        }
                    echo $this->table->generate();
					?>
         
    </div>
</div>
<!-- /page content -->
<?php } ?>