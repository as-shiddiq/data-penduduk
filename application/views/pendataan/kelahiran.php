

<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		<?=$title?></h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
                          
    <?php
        if(isset($_GET['ubah'])){
            $id=$this->input->get('id');
            $this->db->where(['a.id_pendataan'=>$id]);
            $get=$this->pendudukModel->get_dataParamKelahiran()->row_array();
            extract($get);
            $parameter='ubah';
        }
        else{
            $parameter='simpan';
        }
    ?>
	<form class="validate form-horizontal" id="form-penduduk" method="POST" action="<?=site_url()?>penduduk/<?=$parameter?>/<?=$param['param']?>">
				<?=input_hidden('id_penduduk',(isset($id_penduduk)?$id_penduduk:''),'md-input','required');?>	
				<?=input_hidden('parameter',(isset($parameter)?$param['param']:''),'md-input','required');?>	
                
                            <ul  class="nav nav-pills">
                                <li  class="active">
                                    <!-- <a href="#2a" data-toggle="tab">Kelahiran Penduduk</a> -->
                                </li>
                            </ul>
                            <div class="tab-content clearfix">
                          
				            <?=input_hidden('id_pendataan',(isset($id_pendataan)?$id_pendataan:''),'md-input','required');?>	
				            <?=input_hidden('status_pendataan',(isset($status_pendataan)?$status_pendataan:'Baru'),'md-input','required');?>	
				            <?=input_hidden('status',(isset($status_pendataan)?$status_pendataan:'Belum Kawin'),'md-input','required');?>	
				            <?=input_hidden('tanggal',(isset($tanggal)?$tanggal:date('Y-m-d')),'md-input','required');?>	
                            
                            <div class="tab-pane active" id="2a">
                                <div class="row form-status">
                                    <div class="col-md-6">
								<div class="form-group">
									<div class="col-md-3">
									<label>NIK</label>
									</div>
									<div class="col-md-9">
										<?=input_text('nik',(isset($nik)?$nik:''),'md-input','');?>
									</div>
								</div>			
								<div class="form-group">
									<div class="col-md-3">
									<label>Nama Bayi</label>
									</div>
									<div class="col-md-9">
										<?=input_text('nama',(isset($nama)?$nama:''),'md-input','required');?>
									</div>
								</div>						
								<div class="form-group">
									<div class="col-md-3">
									<label>Jenis Kelamin</label>
									</div>
									<div class="col-md-9">
									<?php 
										$op=NULL;
										$op['']='Pilih Salah Satu';  
										$op['Laki-Laki']='Laki-Laki';
										$op['Perempuan']='Perempuan';
											echo select('jk',$op,(isset($jk)?$jk:''),'','required');?>
									</div>
								</div>						
								<div class="form-group">
									<div class="col-md-3">
									<label>Tempat, Tanggal Lahir</label>
									</div>
									<div class="col-md-5">
										<?=input_text('tmpt_lhr',(isset($tmpt_lhr)?$tmpt_lhr:''),'md-input','required');?>
									</div>
									<div class="col-md-4">
										<?=input_date('tgl_lhr',(isset($tgl_lhr)?$tgl_lhr:''),'md-input','required');?>
									</div>
								</div>				
														
								<div class="form-group">
									<div class="col-md-3">
									<label>Alamat</label>
									</div>
									<div class="col-md-9">
										<?=textarea('alamat',(isset($alamat)?$alamat:''),'md-input','');?>
									</div>
								</div>			
<!-- 											
								<div class="form-group">
									<div class="col-md-3">
									<label>Desa</label>
									</div>
									<div class="col-md-9">
									<?php 
										$op=NULL;
										$op['']='Pilih Salah Satu';  
										$this->db->order_by('nama_desa','ASC');
										$data=$this->db->get('desa');
										foreach($data->result() as $row){
											$op[$row->id_desa]=$row->nama_desa;
										}
										echo select('id_desa',$op,(isset($id_desa)?$id_desa:''),'','');?>
									</div>
								</div>	 -->
								<?=input_hidden('id_desa',(isset($id_desa)?$id_desa:''),'md-input','required');?>

								<div class="form-group">
										<div class="col-md-3">
										<label>Orang Tua</label>
										</div>
										<div class="col-md-9">
										<?php 
											$op=NULL;
											$op['']='Pilih Salah Satu';  
											$this->db->order_by('nama','ASC');
											$this->db->where('hub_kel','Kepala Keluarga');
											$data=$this->db->get('penduduk');
											foreach($data->result() as $row){
												$op[$row->id_penduduk]=$row->nama;
											}
											echo select('master_id',$op,(isset($master_id)?$master_id:''),'','');?>
										</div>
								</div>	
								</div>
								<div class="col-md-6" >
									<div class="form-group">
										<div class="col-md-3">
										<label>Golongan darah</label>
										</div>
										<div class="col-md-9">
										<?php 
											$op=NULL;
											$op['']='Pilih Salah Satu';  
											$op['A']='A';
											$op['B']='B';
											$op['AB']='AB';
											$op['O']='O';
											echo select('goldar',$op,(isset($goldar)?$goldar:''),'','');?>
										</div>
									</div>						
									<div class="form-group">
										<div class="col-md-3">
										<label>Agama</label>
										</div>
										<div class="col-md-9">
										<?php 
											$op=NULL;
											$op['']='Pilih Salah Satu';  
											$op['Islam']='Islam';
											$op['Kristen']='Kristen';
											$op['Katolik']='Katolik';
											$op['Hindu']='Hindu';
											$op['Budha']='Budha';
											echo select('agama',$op,(isset($agama)?$agama:''),'','');?>
										</div>
									</div>								
									<div class="form-group">
										<div class="col-md-3">
										<label>Nama Ibu</label>
										</div>
										<div class="col-md-9">
											<?=input_text('nama_ibu',(isset($nama_ibu)?$nama_ibu:''),'md-input','');?>
										</div>
									</div>						
									<div class="form-group">
										<div class="col-md-3">
										<label>Nama Ayah</label>
										</div>
										<div class="col-md-9">
											<?=input_text('nama_ayah',(isset($nama_ayah)?$nama_ayah:''),'md-input','');?>
										</div>
									</div>	

									<div class="form-group">
										<div class="col-md-3">
										<label>Status Kelahiran</label>
										</div>
										<div class="col-md-9">
										<?php 
											$op=NULL;
											$op['']='Pilih Salah Satu';  
											$op['Normal']='Normal';
											$op['Caesar']='Caesar';
											$op['Prematur']='Prematur';
											echo select('pendataan',$op,(isset($pendataan)?$pendataan:''),'','');?>
										</div>
									</div>						
									<div class="form-group">
										<div class="col-md-3">
										<label>Nama Bidan</label>
										</div>
										<div class="col-md-9">
											<?=input_text('keterangan',(isset($keterangan)?$keterangan:''),'md-input','');?>
										</div>
									</div>					
									<div class="form-group">
										<div class="col-md-3">
										<label>Tempat Persalinan</label>
										</div>
										<div class="col-md-9">
											<?=input_text('keterangan2',(isset($keterangan2)?$keterangan2:''),'md-input','');?>
										</div>
									</div>							
									</div>		
                                </div>
                                </div>
						<div class="clearfix"></div>
						</div>
                        <div class="col-md-12" style="background: #fff;border: 1px solid #eee;padding-top: 10px;">
                            <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                            <a href="<?=site_url('penduduk/index/'.$param['param'])?>" class="btn btn-danger">Kembali</a>
                        </div>
		</form>
    </div>
</div>
<?php } else { ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		<?=$title?></h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">

					<!-- table -->
					<a href="<?=site_url('penduduk/index/'.$param['param'].'?tambah')?>" class="btn btn-success btn-tambah">
						<i class='fa fa-plus'></i> Tambah
					</a>
					<hr>
					 <form>
                        <?php
                            $bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
                            $tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
                        ?>
                        <div class="row">
                            <div class="col-md-4">
                                <?=select('bulan',bulanList(),$bulan)?>
                            </div>
                            <div class="col-md-4">
                                <?=select('tahun',tahunList(),$tahun)?>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-info">Lihat</button>
                                <a href="<?=site_url('cetak')?>?jenis=kelahiran&bulan=<?=$bulan?>&tahun=<?=$tahun?>" class="btn btn-warning" target="_blank">Cetak</a>
                            </div>
                        </div>
                    </form>
                    <hr>
					<?php echo $this->session->flashdata('info');?>
					<!-- end table -->
					<?php
					$template = array(
                            'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
                        );
                        $this->table->set_template($template);
                        $this->table->set_heading('No','NIK','Nama','TTL','Jenis Kelamin','Kelahiran','Bidan','Tempat Persalinan','Tanggal','');
                        $i=1;
                         if($bulan!=''){
                            $this->db->where("DATE_FORMAT(tanggal,'%m')='".$bulan."'");
                        }
                        if($tahun!=''){
                            $this->db->where("DATE_FORMAT(tanggal,'%Y')='".$tahun."'");
                        }
						$this->db->where($param['where']);
						$get_data=$this->pendudukModel->get_dataParamKelahiran();
                        foreach($get_data->result() as $row){
                            $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                                        $row->nik,
                                                                        $row->nama,
                                                                        $row->tmpt_lhr.'/'.standar_tanggal($row->tgl_lhr),
                                                                        $row->jk,
                                                                        $row->pendataan,
                                                                        $row->keterangan,
                                                                        $row->keterangan2,
                                                                        standar_tanggal($row->tanggal),
                                                                        array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                                        <li>'.anchor(site_url("penduduk/index/".$param['param']."?ubah&id=".$row->id_pendataan),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                                        <li>'.anchor(site_url("penduduk/hapuspendataan/".$param['param']."?id_pendataan=".$row->id_pendataan),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                                        </ul>',"width"=>"20px","align"=>"center"));
                            $i++;
                        }
                    echo $this->table->generate();
					?>
         
    </div>
</div>
<!-- /page content -->
<?php } ?>