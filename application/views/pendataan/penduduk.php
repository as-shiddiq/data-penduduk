
<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		DATA PENDUDUK</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
	<?php
		if(isset($_GET['ubah'])){
			$id=$this->input->get('id');
			$this->db->where(['a.id_penduduk'=>$id]);
			$get=$this->pendudukModel->get_data()->row_array();
			extract($get);
			$parameter='ubah';

		}
		else{
			$parameter='simpan';
		}
	?>

	<form class="validate form-horizontal" id="form-penduduk" method="POST" action="<?=site_url()?>penduduk/<?=$parameter?>">
				<?=input_hidden('id_penduduk',(isset($id_penduduk)?$id_penduduk:''),'md-input','required');?>	
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-3">
						<label>NIK</label>
						</div>
						<div class="col-md-9">
							<?=input_text('nik',(isset($nik)?$nik:''),'md-input','required');?>
						</div>
					</div>			
					<div class="form-group">
						<div class="col-md-3">
						<label>Nama Penduduk</label>
						</div>
						<div class="col-md-9">
							<?=input_text('nama',(isset($nama)?$nama:''),'md-input','required');?>
						</div>
					</div>						
					<div class="form-group">
						<div class="col-md-3">
						<label>Jenis Kelamin</label>
						</div>
						<div class="col-md-9">
						<?php 
							$op=NULL;
							$op['']='Pilih Salah Satu';  
							$op['Laki-Laki']='Laki-Laki';
							$op['Perempuan']='Perempuan';
								echo select('jk',$op,(isset($jk)?$jk:''),'','required');?>
						</div>
					</div>						
					<div class="form-group">
						<div class="col-md-3">
						<label>Tempat, Tanggal Lahir</label>
						</div>
						<div class="col-md-5">
							<?=input_text('tmpt_lhr',(isset($tmpt_lhr)?$tmpt_lhr:''),'md-input','required');?>
						</div>
						<div class="col-md-4">
							<?=input_date('tgl_lhr',(isset($tgl_lhr)?$tgl_lhr:''),'md-input','required');?>
						</div>
					</div>				
					<hr>
					<div class="form-group">
						<div class="col-md-3">
						<label>Alamat</label>
						</div>
						<div class="col-md-9">
							<?=textarea('alamat',(isset($alamat)?$alamat:''),'md-input','');?>
						</div>
					</div>	
					<!--		
					<div class="form-group">
					<div class="col-md-3">
						<label>Provinsi</label>
					</div>
					<div class="col-md-9">
						<?php 
							$op=NULL;
							$op['']='Pilih Salah Satu';  
							$this->db->order_by('nama_provinsi','ASC');
							$data=$this->db->get('provinsi');
							foreach($data->result() as $row){
							$op[$row->id_provinsi]=$row->nama_provinsi;
							}
							echo select('id_provinsi',$op,(isset($id_provinsi)?$id_provinsi:''),'get-kabupaten','');?>
					</div>
					</div>
							
					<div class="form-group load-kabupaten">
					<div class="col-md-3">
							<label>Kabupaten/Kota</label>

					</div>
					<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$this->db->order_by('nama_kabupaten','ASC');
								$data=$this->db->get('kabupaten');
								foreach($data->result() as $row){
									$op[$row->id_kabupaten]=$row->nama_kabupaten;
								}
								echo select('id_kabupaten',$op,(isset($id_kabupaten)?$id_kabupaten:''),'get-kecamatan','');?>
					</div>
					</div>
														
					<div class="form-group load-kecamatan">
					<div class="col-md-3">
							<label>Kecamatan</label>
					</div>
					<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$this->db->order_by('nama_kecamatan','ASC');
								$data=$this->db->get('kecamatan');
								foreach($data->result() as $row){
									$op[$row->id_kecamatan]=$row->nama_kecamatan;
								}
								echo select('id_kecamatan',$op,(isset($id_kecamatan)?$id_kecamatan:''),'get-desa','');?>
					</div>
					</div>
								
														
					<div class="form-group load-desa">
					<div class="col-md-3">
							<label>Kelurahan/Desa</label>
					</div>
					<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$this->db->order_by('nama_desa','ASC');
								$data=$this->db->get('desa');
								foreach($data->result() as $row){
									$op[$row->id_desa]=$row->nama_desa;
								}
								echo select('id_desa',$op,(isset($id_desa)?$id_desa:''),'','required');?>
					</div>
					</div>			
					-->
					<?=input_hidden('id_desa',(isset($id_desa)?$id_desa:''),'md-input','required');?>

					</div>
					<div class="col-md-6" >
						<div class="form-group">
							<div class="col-md-3">
							<label>Pendidikan</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$this->db->order_by('nama_pendidikan','ASC');
								$data=$this->db->get('pendidikan');
								foreach($data->result() as $row){
									$op[$row->id_pendidikan]=$row->nama_pendidikan;
								}
									echo select('id_pendidikan',$op,(isset($id_pendidikan)?$id_pendidikan:''),'','required');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Pekerjaan</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$this->db->order_by('nama_pekerjaan','ASC');
								$data=$this->db->get('pekerjaan');
								foreach($data->result() as $row){
									$op[$row->id_pekerjaan]=$row->nama_pekerjaan;
								}
									echo select('id_pekerjaan',$op,(isset($id_pekerjaan)?$id_pekerjaan:''),'','required');?>
							</div>
						</div>		
						
						<div class="form-group">
							<div class="col-md-3">
							<label>Golongan Darah</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['A']='A';
								$op['B']='B';
								$op['AB']='AB';
								$op['O']='O';
									echo select('goldar',$op,(isset($goldar)?$goldar:''),'','');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Agama</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['Islam']='Islam';
								$op['Kristen']='Kristen';
								$op['Katolik']='Katolik';
								$op['Hindu']='Hindu';
								$op['Budha']='Budha';
									echo select('agama',$op,(isset($agama)?$agama:''),'','required');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Status</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['Kawin']='Kawin';
								$op['Belum Kawin']='Belum Kawin';
								$op['Janda']='Janda';
								$op['Duda']='Duda';
								echo select('status',$op,(isset($status)?$status:''),'','required');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Hubungan Keluarga</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['Kepala Keluarga']='Kepala Keluarga';
								$op['Istri']='Istri';
								$op['Anak']='Anak';
								echo select('hub_kel',$op,(isset($hub_kel)?$hub_kel:''),'','');?>
							</div>
						</div>		
						<div class="form-group hub-kel-dgn">
								<div class="col-md-3">
								<label>Hubungan Dengan <span class="text-danger">*)</span></label>
								</div>
								<div class="col-md-9">
								<?php 
									$op=NULL;
									$op['']='Pilih Salah Satu';  
									$this->db->order_by('nama','ASC');
									$data=$this->db->get('penduduk');
									foreach($data->result() as $row){
										$op[$row->id_penduduk]=$row->nama;
									}
										echo select('master_id',$op,(isset($master_id)?$master_id:''),'','');?>
								</div>
						</div>					
						<div class="form-group">
							<div class="col-md-3">
							<label>Nama Ibu</label>
							</div>
							<div class="col-md-9">
								<?=input_text('nama_ibu',(isset($nama_ibu)?$nama_ibu:''),'md-input','required');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Nama Ayah</label>
							</div>
							<div class="col-md-9">
								<?=input_text('nama_ayah',(isset($nama_ayah)?$nama_ayah:''),'md-input','required');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Keterangan</label>
							</div>
							<div class="col-md-9">
								<?=textarea('keterangan',(isset($keterangan)?$keterangan:''),'md-input','');?>
							</div>
						</div>					
						</div>					
						<div class="clearfix"></div>
						<div class="col-md-12">
										
						<button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
						<a href="<?=site_url('penduduk')?>" class="btn btn-danger">Kembali</a>
						</div>
						
		</form>
    </div>
</div>
<?php } else { ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		DATA PENDUDUK</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">

					<!-- table -->
					<a href="<?=site_url('penduduk?tambah')?>" class="btn btn-success btn-tambah">
						<i class='fa fa-plus'></i> Tambah
					</a>
					
					<hr>
                    <form>
                        <?php
                            $bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
                            $tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
                        ?>
                        <div class="row">
                            <div class="col-md-4">
                                <?=select('bulan',bulanList(),$bulan)?>
                            </div>
                            <div class="col-md-4">
                                <?=select('tahun',tahunList(),$tahun)?>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-info">Lihat</button>
                                <a href="<?=site_url('cetak')?>?jenis=penduduk&bulan=<?=$bulan?>&tahun=<?=$tahun?>" class="btn btn-warning" target="_blank">Cetak</a>
                            </div>
                        </div>
                    </form>
					<hr>

					<?php echo $this->session->flashdata('info');?>
					<?=$msg;?>
					<!-- end table -->
					<?php
					$template = array(
							'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
						);
						$this->table->set_template($template);
						$this->table->set_heading('No','NIK','Nama','Jenis kelamin','Tempat, Tanggal Lahir','Golongan Darah','Agama','status','Hubungan Keluarga','Nama ibu','Nama ayah','');
						$i=1;
						foreach($get_data->result() as $row){
							$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
																		$row->nik,
																		$row->nama,
																		$row->jk,
																		$row->tmpt_lhr.', '.standar_tanggal($row->tgl_lhr),
																		$row->goldar,
																		$row->agama,
																		$row->status,
																		$row->hub_kel,
																		$row->nama_ibu,
																		$row->nama_ayah,
																		array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
																		<li>'.anchor(site_url("penduduk?ubah&id=".$row->id_penduduk),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
																		<li>'.anchor(site_url("penduduk/hapus?id_penduduk=".$row->id_penduduk),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
																		</ul>',"width"=>"20px","align"=>"center"));
							$i++;
						}
						echo $this->table->generate();
					?>
         
    </div>
</div>
<!-- /page content -->
<?php } ?>