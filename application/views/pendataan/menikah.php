
<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		<?=$title?></h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
	<?php
		if(isset($_GET['ubah'])){
			$id=$this->input->get('id');
            $this->db->where(['a.id_pendataan'=>$id]);
            $get=$this->pendudukModel->get_dataParamPindahDomisili()->row_array();
			extract($get);
			$parameter='ubah';
		}
		else{
			$parameter='simpan';
		}
	?>

    <form class="validate form-horizontal" id="form-penduduk" method="POST" action="<?=site_url()?>penduduk/<?=$parameter?>/<?=$param['param']?>">
				<?=input_hidden('id_penduduk',(isset($id_penduduk)?$id_penduduk:''),'md-input','required');?>	
				<?=input_hidden('parameter',(isset($parameter)?$param['param']:''),'md-input','required');?>	
                
                            <ul  class="nav nav-pills">
                                <li  class="active">
                                    <!-- <a href="#2a" data-toggle="tab">Pernikahan</a> -->
                                </li>
                            </ul>
                            <div class="tab-content clearfix">
                            <div class="tab-pane" id="1a">
                				<div class="row form-penduduk">
					            <?php //include('include/forpenduduk.php')?>
                                </div>
                            </div>
                                                    
                           
                            <?=input_hidden('id_pendataan',(isset($id_pendataan)?$id_pendataan:''),'md-input','required');?>    
				            <?=input_hidden('status_pendataan',(isset($status_pendataan)?$status_pendataan:'Perubahan'),'md-input','required');?>	
                            
                            <div class="tab-pane active" id="2a">
                                <div class="row form-status">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>NIK Suami</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?php 
                                                $op=NULL;
                                                $op['']='Pilih Salah Satu';  
                                                $this->db->order_by('nama','ASC');
                                                $this->db->where('jk','Laki-laki');
                                                $data=$this->db->get('penduduk');
                                                foreach($data->result() as $row){
                                                    $op[$row->id_penduduk]=$row->nik;
                                                }
                                                echo select('id_penduduk',$op,(isset($id_penduduk)?$id_penduduk:''),'get-nik','required');?>
                                            </div>
                                        </div>          
                                    </div>
                                    <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label></label>
                                        </div>
                                        <div class="col-md-9">
                                            <b>Detail Penduduk</b>
                                            <div class="load-nik" style="background: #efe">
                                            <?=info_warning('Pilih Nik terlebih dahulu')?>
                                            </div>
                                        </div>
                                    </div>   
                                    </div>   
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>NIK Istri</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?php 
                                                $op=NULL;
                                                $op['']='Pilih Salah Satu';  
                                                $this->db->order_by('nama','ASC');
                                                $this->db->where('jk','Perempuan');
                                                $data=$this->db->get('penduduk');
                                                foreach($data->result() as $row){
                                                    $op[$row->id_penduduk]=$row->nik;
                                                }
                                                echo select('id1',$op,(isset($id1)?$id1:''),'get-nik2','required');?>
                                            </div>
                                        </div>          
                                    </div>
                                    <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label></label>
                                        </div>
                                        <div class="col-md-9">
                                            <b>Detail Penduduk</b>
                                            <div class="load-nik2" style="background: #efe">
                                            <?=info_warning('Pilih Nik terlebih dahulu')?>
                                            </div>
                                        </div>
                                    </div>   
                                    </div>   

                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>Nama Penghulu</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?=input_text('pendataan',(isset($pendataan)?$pendataan:''),'','required');?>
                                            </div>
                                        </div>          
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>Lokasi Akad</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?=input_text('keterangan',(isset($keterangan)?$keterangan:''),'','required');?>
                                            </div>
                                        </div>          
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>Saksi Nikah</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?=textarea('keterangan2',(isset($keterangan2)?$keterangan2:''),'','required');?>
                                            </div>
                                        </div>          
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                            <label>Tanggal Pernikahan</label>
                                            </div>
                                            <div class="col-md-9">
                                            <?=input_date('tanggal',(isset($tanggal)?$tanggal:date('Y-m-d')),'','required');?>
                                            </div>
                                        </div>			
                                    </div>
                                </div>
                                </div>
						<div class="clearfix"></div>
						</div>
                        <div class="col-md-12" style="background: #fff;border: 1px solid #eee;padding-top: 10px;">
                            <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                            <a href="<?=site_url('penduduk/index/'.$param['param'])?>" class="btn btn-danger">Kembali</a>
                        </div>
		</form>
    </div>
</div>
<?php } else { ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		<?=$title?></h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">

					<!-- table -->
					<a href="<?=site_url('penduduk/index/'.$param['param'].'?tambah')?>" class="btn btn-success btn-tambah">
						<i class='fa fa-plus'></i> Tambah
					</a>
					<hr>
                     <form>
                        <?php
                            $bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
                            $tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
                        ?>
                        <div class="row">
                            <div class="col-md-4">
                                <?=select('bulan',bulanList(),$bulan)?>
                            </div>
                            <div class="col-md-4">
                                <?=select('tahun',tahunList(),$tahun)?>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-info">Lihat</button>
                                <a href="<?=site_url('cetak')?>?jenis=menikah&bulan=<?=$bulan?>&tahun=<?=$tahun?>" class="btn btn-warning" target="_blank">Cetak</a>
                            </div>
                        </div>
                    </form>
                    <hr>
					<?php echo $this->session->flashdata('info');?>
					<!-- end table -->
					<?php
					$template = array(
                            'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
                        );
                        $this->table->set_template($template);
                        $this->table->set_heading('No','Mempelai Pria','Mempelai Wanita','Penghulu','Lokasi Akad','Saksi','tanggal','');
                        $i=1;
                         if($bulan!=''){
                            $this->db->where("DATE_FORMAT(tanggal,'%m')='".$bulan."'");
                        }
                        if($tahun!=''){
                            $this->db->where("DATE_FORMAT(tanggal,'%Y')='".$tahun."'");
                        }
                        $this->db->where($param['where']);
                        $get_data=$this->pendudukModel->get_dataParamMenikah();
                        foreach($get_data->result() as $row){
                            $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                                        $row->nama,
                                                                        $row->pasangan,
                                                                        $row->pendataan,
                                                                        $row->keterangan,
                                                                        $row->keterangan2,
                                                                        standar_tanggal($row->tanggal),
                                                                         array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                                        <li>'.anchor(site_url("penduduk/index/".$param['param']."?ubah&id=".$row->id_pendataan),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                                        <li>'.anchor(site_url("penduduk/hapuspendataan/".$param['param']."?id_pendataan=".$row->id_pendataan),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                                        </ul>',"width"=>"20px","align"=>"center"));
                            $i++;
                        }
                    echo $this->table->generate();
					?>
         
    </div>
</div>
<!-- /page content -->
<?php } ?>