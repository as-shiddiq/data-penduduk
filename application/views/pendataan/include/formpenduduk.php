<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-3">
						<label>NIK</label>
						</div>
						<div class="col-md-9">
							<?=input_text('nik',(isset($nik)?$nik:''),'md-input','');?>
						</div>
					</div>			
					<div class="form-group">
						<div class="col-md-3">
						<label>Nama Penduduk</label>
						</div>
						<div class="col-md-9">
							<?=input_text('nama',(isset($nama)?$nama:''),'md-input','');?>
						</div>
					</div>						
					<div class="form-group">
						<div class="col-md-3">
						<label>Jkel</label>
						</div>
						<div class="col-md-9">
						<?php 
							$op=NULL;
							$op['']='Pilih Salah Satu';  
							$op['Laki-Laki']='Laki-Laki';
							$op['Perempuan']='Perempuan';
								echo select('jk',$op,(isset($jk)?$jk:''),'','');?>
						</div>
					</div>						
					<div class="form-group">
						<div class="col-md-3">
						<label>TTL</label>
						</div>
						<div class="col-md-5">
							<?=input_text('tmpt_lhr',(isset($tmpt_lhr)?$tmpt_lhr:''),'md-input','');?>
						</div>
						<div class="col-md-4">
							<?=input_date('tgl_lhr',(isset($tgl_lhr)?$tgl_lhr:''),'md-input','');?>
						</div>
					</div>				
											
					<div class="form-group">
						<div class="col-md-3">
						<label>Alamat</label>
						</div>
						<div class="col-md-9">
							<?=textarea('alamat',(isset($alamat)?$alamat:''),'md-input','');?>
						</div>
					</div>			
								
					<div class="form-group">
						<div class="col-md-3">
						<label>Desa</label>
						</div>
						<div class="col-md-9">
						<?php 
							$op=NULL;
							$op['']='Pilih Salah Satu';  
							$this->db->order_by('nama_desa','ASC');
							$data=$this->db->get('desa');
							foreach($data->result() as $row){
								$op[$row->id_desa]=$row->nama_desa;
							}
								echo select('id_desa',$op,(isset($id_desa)?$id_desa:''),'','');?>
						</div>
					</div>					
					<div class="form-group">
						<div class="col-md-3">
						<label>Pendidikan</label>
						</div>
						<div class="col-md-9">
						<?php 
							$op=NULL;
							$op['']='Pilih Salah Satu';  
							$this->db->order_by('nama_pendidikan','ASC');
							$data=$this->db->get('pendidikan');
							foreach($data->result() as $row){
								$op[$row->id_pendidikan]=$row->nama_pendidikan;
							}
								echo select('id_pendidikan',$op,(isset($id_pendidikan)?$id_pendidikan:''),'','');?>
						</div>
					</div>						
					<div class="form-group">
						<div class="col-md-3">
						<label>Pekerjaan</label>
						</div>
						<div class="col-md-9">
						<?php 
							$op=NULL;
							$op['']='Pilih Salah Satu';  
							$this->db->order_by('nama_pekerjaan','ASC');
							$data=$this->db->get('pekerjaan');
							foreach($data->result() as $row){
								$op[$row->id_pekerjaan]=$row->nama_pekerjaan;
							}
								echo select('id_pekerjaan',$op,(isset($id_pekerjaan)?$id_pekerjaan:''),'','');?>
						</div>
					</div>		
					<div class="form-group">
							<div class="col-md-3">
							<label>Hub KK dgn</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$this->db->order_by('nama','ASC');
								$data=$this->db->get('penduduk');
								foreach($data->result() as $row){
									$op[$row->id_penduduk]=$row->nama;
								}
									echo select('master_id',$op,(isset($master_id)?$master_id:''),'','');?>
							</div>
					</div>	
					</div>
					<div class="col-md-6" >
						<div class="form-group">
							<div class="col-md-3">
							<label>Goldar</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['A']='A';
								$op['B']='B';
								$op['AB']='AB';
								$op['O']='O';
									echo select('goldar',$op,(isset($goldar)?$goldar:''),'','');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Agama</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['Islam']='Islam';
								$op['Kristen']='Kristen';
								$op['Katolik']='Katolik';
								$op['Hindu']='Hindu';
								$op['Budha']='Budha';
									echo select('agama',$op,(isset($agama)?$agama:''),'','');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Status</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['Kawin']='Kawin';
								$op['Belum Kawin']='Belum Kawin';
								$op['Janda']='Janda';
								$op['Duda']='Duda';
								echo select('status',$op,(isset($status)?$status:''),'','');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Hub Kel</label>
							</div>
							<div class="col-md-9">
							<?php 
								$op=NULL;
								$op['']='Pilih Salah Satu';  
								$op['Kepala Keluarga']='Kepala Keluarga';
								$op['Istri']='Istri';
								$op['Anak']='Anak';
								echo select('hub_kel',$op,(isset($hub_kel)?$hub_kel:''),'','');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Nama Ibu</label>
							</div>
							<div class="col-md-9">
								<?=input_text('nama_ibu',(isset($nama_ibu)?$nama_ibu:''),'md-input','');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Nama Ayah</label>
							</div>
							<div class="col-md-9">
								<?=input_text('nama_ayah',(isset($nama_ayah)?$nama_ayah:''),'md-input','');?>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-md-3">
							<label>Keterangan</label>
							</div>
							<div class="col-md-9">
								<?=textarea('keterangan',(isset($keterangan)?$keterangan:''),'md-input','');?>
							</div>
						</div>					
						</div>					