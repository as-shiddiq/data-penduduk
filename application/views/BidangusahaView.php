	<!-- form -->
<!-- The Modal -->
<div class="modal fade" id="FormModal">
<form class="validate form-horizontal" id="form-bidangusaha" method="POST">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form Bidang Usaha</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <!-- atur sesuai field di database -->
      <div class="modal-body">
        <?=input_hidden('id_bidang_usaha',(isset($id_bidang_usaha)?$id_bidang_usaha:''),'md-input','required');?>						
        <div class="form-group">

          <div class="col-md-3">
              <label>Bidang Usaha</label>
          </div>
          <div class="col-md-9">
              <?=input_text('bidang_usaha',(isset($bidang_usaha)?$bidang_usaha:''),'md-input','required');?>
          </div>
          
        </div>
<!--endform-->
      </div>
       <!-- Modal footer -->
      <div class="modal-footer">
         <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</form>
</div><!--OPEN TABLE-->
<!-- page content -->

<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		DATA BIDANG USAHA</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">

					<!-- table -->
					<button type="button" class="btn btn-success btn-tambah" data-toggle="modal" data-target="#FormModal">
						<i class='fa fa-plus'></i> Tambah
					</button>					
					<!-- <a href="<?=site_url($config['url'].'/cetak')?>" target="_blank" class="btn btn-danger"><i class="fa fa-print"></i> Cetak</a> -->

					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
         
    </div>
</div>
<!-- /page content -->


