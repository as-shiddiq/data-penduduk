<script src="<?=templates()?>js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?=templates()?>js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?=base_url()?>publics/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>publics/select2/dist/js/select2.min.js"></script>
<script src="<?=templates()?>js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="<?=templates()?>js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
<script src="<?=templates()?>js/jquery.nicescroll.min.js" type="text/javascript"></script>
<script src="<?=templates()?>js/jquery.nicescroll.plus.js" type="text/javascript"></script>
<script src="<?=templates()?>js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="<?=templates()?>js/stellar.js" type="text/javascript"></script>
<script src="<?=templates()?>js/parsley.js" type="text/javascript"></script>
<script src="<?=templates()?>js/counter.js" type="text/javascript"></script>
<script src="<?=templates()?>js/smooth-scroll.js" type="text/javascript"></script>
<script src="<?=templates()?>js/jquery.isotope.min.js" type="text/javascript"></script>
<script src="<?=templates()?>js/jquery.cre-animate.js" type="text/javascript"></script>
<!-- <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script> -->
<script src="<?=templates()?>js/custom.js" type="text/javascript"></script>
<!-- ================================================== -->
<!-- ================ END JQUERY SCRIPTS ================= -->
<!-- ================================================== -->
<!-- ================================================== -->
<!-- ============= START SCROLL TO TOP SCRIPT ============= -->
<!-- ================================================== -->
<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>
<script type="text/javascript">
	 $("select").select2({
       width: 'resolve' ,
    });
    $('#table-dt').DataTable({
      "paging": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
    $(document).on("change",".get-kabupaten",function(){
    var val=$(this).val(),
        load=$(this).data('select');
    if(val!=''){
        $(".load-kabupaten").load('<?=base_url('ajax/getKabupaten/')?>'+val+'/'+load);
    }
    })
    $(document).on("change",".get-kecamatan",function(){
    var val=$(this).val(),
        load=$(this).data('select');
    if(val!=''){
        $(".load-kecamatan").load('<?=base_url('ajax/getKecamatan/')?>'+val+'/'+load);
    }
    })
    $(document).on("change",".get-desa",function(){
    var val=$(this).val(),
        load=$(this).data('select');
    if(val!=''){
        $(".load-desa").load('<?=base_url('ajax/getDesa/')?>'+val+'/'+load);
    }
    })
</script>