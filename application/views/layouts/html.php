<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title?></title>
    <?php include 'head.php';?>
  <body>
  </head>
    <div id="pageloader">
      <div class="loader-img">
        <img alt="loader" src="<?=base_url('publics/images/logotala.png')?>" width="60px" /> 
        <p style="color:#fff">Memuat halaman...</p>
      </div>
    </div>
      <?php include 'menu.php';?>
      <section id="blog-page">
      <div class="fullwidth-section">
        <div class="container">
         <?php echo $body;?>
        </div>
      </section>
      <?php include 'footer.php'?>
      <?php include 'js.php'?>
      <?php
        if(isset($js)){
          echo $js;
        }
      ?>
  </body>
</html>
