<link href="<?=templates()?>css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?=templates()?>css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?=templates()?>css/icomoon.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?=templates()?>css/prettyPhoto.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?=templates()?>css/slider-settings.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?=templates()?>css/haze-buttons.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?=templates()?>css/menu.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?=templates()?>css/haze.css" media="screen" rel="stylesheet" type="text/css">


<link rel="stylesheet" href="<?=base_url()?>publics/datatables/jquery.dataTables.custom.css">
<link rel="stylesheet" href="<?=base_url()?>publics/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=base_url()?>publics/select2/dist/css/select2.min.css">


<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800);
    @import url(http://fonts.googleapis.com/css?family=Roboto:400,300,700,500,900);
    @import url(http://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700);
    @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,600,900);
    .dropdown-menu {
        position: absolute;
        left: inherit !important;
        top: inherit !important;
        float:none;
        margin-top:-10px;
        border-radius:0;
        margin-left:2px
    }
    .btn{
        border-radius:0
    }
    .dropdown-menu li{
        text-align:left !important
    }
    h1{
        background:#CF014A;
        color:#fff;
        padding:10px 20px;
        font-size:20px;
        width:auto;
        font-weight:bold !important;
    }
    form-group input, .form-group select, 
    form-group textarea {
    background-color: rgba(0,0,0,0);
    color: #777;
    border: 2px solid #E2E2E2;
    padding: 5px 10px;
    height: auto;
    font-size: 13px;
    box-shadow: none;
}
.nav.nav-pills{
    margin-bottom:10px;
    border-bottom:1px solid #eee;
    padding-bottom:10px
}
.nav-pills>li>a{
    border-radius:0
} 
  .select2-container--default .select2-selection--multiple .select2-selection__choice,
  .select2-container--default .select2-selection--single{
    border-radius: 0 ;
    border-color:#ddd;
    height: 38px;
    padding: 6px 4px;
  }
  .select2 .selection ,.select2{
    width: 100% !important;
  }
  .select2 .selection span{
    font-size: 12px
  }
  .select2-results{
    background: #fff
  }
  ul.drop-down{
      position:absolute;
      width:100%
  }
  .header-me{
      padding:10px;
      background:#01CFCA
  }
  .header-me img{
      float:left;
      margin:4px 10px 
  }
  .header-me h1{
      background:none;
      padding:0;
      margin:0;
      font-size:14px
  }
  .header-me h2{
      background:none;
      padding:0;
      margin:0;
      color:#fff;
      font-weight:bold
  }
  .header-me p{
      background:none;
      color:#fff;
      padding:0;
      margin:0
  }
  .fullwidth-section{
      padding-top:150px
  }
    .loader-img {
        position: absolute;
        width: 20%;
        margin: 20% 40%;
        text-align: center;
        top: 0;
        left: 0;
    }
  
</style>