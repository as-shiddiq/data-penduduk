<div class="top-menu" style="position:absolute;background:#01CFCA;height:120px">
<header class="header-me container">
<img src="<?=base_url('publics/images/logotala.png')?>" style="width:60px">
<h1>SISTEM INFORMASI DATA PENDUDUK</h1>
<h2>DESA KANDANGAN LAMA</h2>
<p>Jl. Pendidikan RT.06/RW.03 Kandangan Lama Kode Pos 70782</p>
</header>
	<div id="navigation-button">
		<i class="fa fa-bars fa-fw"></i></div>
	<ul class="nav drop-down">
    <div class="container">
        <li class="active"><a href="<?=site_url('')?>">Beranda</a></li>
        <?php if ($this->session->userdata('level')=='Administrator'): ?>
		<li><a href="#">Master Data</a>
            <ul class="sub-menu">
                <!-- <li><a href="<?=site_url('provinsi')?>">Data Provinsi</a></li>
                <li><a href="<?=site_url('kabupaten')?>">Data Kabupaten</a></li>
                <li><a href="<?=site_url('kecamatan')?>">Data Kecamatan</a></li>
                <li><a href="<?=site_url('desa')?>">Data Desa</a></li> -->
                <li><a href="<?=site_url('pekerjaan')?>">Data Pekerjaan</a></li>
                <li><a href="<?=site_url('pendidikan')?>">Data Pendidikan</a></li>
                <li><a href="<?=site_url('bidangusaha')?>">Data Bidang Usaha</a></li>
                <li><a href="<?=site_url('pengguna')?>">Data Pengguna</a></li>
            </ul>
        </li>
        <?php endif ?>
		<li><a href="<?=site_url('penduduk')?>">Data Penduduk</a></li>
		<li><a href="#">Pendataan</a>
            <ul class="sub-menu">
                <li><a href="<?=site_url('penduduk/index/kelahiran')?>">Kelahiran</a></li>
                <li><a href="<?=site_url('penduduk/index/kematian')?>">Kematian</a></li>
                <li><a href="<?=site_url('penduduk/index/kurangmampu')?>">Kurang Mampu</a></li>
                <li><a href="<?=site_url('penduduk/index/sudahektp')?>">Status E-KTP</a></li>
                <li><a href="<?=site_url('penduduk/index/ijinusaha')?>">Ijin Usaha</a></li>
                <li><a href="<?=site_url('penduduk/index/pindahdomisili')?>">Pindah Domisili</a></li>
                <li><a href="<?=site_url('penduduk/index/menikah')?>">Menikah</a></li>
                <li><a href="<?=site_url('penduduk/index/cerai')?>">Cerai</a></li>
            </ul>
        </li>
        <li><a href="<?=site_url('suratkeluar')?>">Surat Keluar</a></li>
        <li><a href="<?=site_url('login/logout')?>">Keluar</a></li>
        <li>&nbsp;</li>
    </div>

	</ul>
</div>