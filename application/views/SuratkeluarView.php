	<!-- form -->
<!-- The Modal -->
<div class="modal fade" id="FormModal">
<form class="validate form-horizontal" id="form-suratkeluar" method="POST">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form Surat Keluar</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <?=input_hidden('id_surat_masuk',(isset($id_surat_masuk)?$id_surat_masuk:''),'md-input','required');?>						
		<div class="form-group">
		<div class="col-md-3">
				<label>Nomor Surat</label>
		</div>
		<div class="col-md-9">
				<?=input_text('no_surat',(isset($no_surat)?$no_surat:''),'md-input','required');?>
		</div>
		</div>
								
		<div class="form-group">
		<div class="col-md-3">
				<label>Tujuan</label>
		</div>
		<div class="col-md-9">
				<?=input_text('tujuan',(isset($tujuan)?$tujuan:''),'md-input','required');?>
		</div>
		</div>
								
		<div class="form-group">
		<div class="col-md-3">
				<label>Perihal</label>
		</div>
		<div class="col-md-9">
				<?=textarea('perihal',(isset($perihal)?$perihal:''),'md-input','required');?>
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3">
				<label>Keterangan</label>
		</div>
		<div class="col-md-9">
				<?=textarea('keterangan',(isset($keterangan)?$keterangan:''),'md-input','required');?>
		</div>
		</div>
								
		<div class="form-group">
		<div class="col-md-3">
				<label>Sifat</label>
		</div>
		<div class="col-md-9">
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					$op['Biasa']='Biasa';
					$op['Penting']='Penting';
					$op['Sangat Penting']='Sangat Penting';
					$op['Segera']='Segera';
					echo select('sifat',$op,(isset($sifat)?$sifat:''),'','required');?>
		</div>
		</div>
								
		<div class="form-group">
		<div class="col-md-3">
				<label>Tanggal Surat</label>
		</div>
		<div class="col-md-9">
		<?=input_date('tanggal_surat',(isset($tanggal_surat)?$tanggal_surat:date('Y-m-d')),'md-input','required');?>
		</div>
		</div>
								
		<?=input_hidden('no_agenda',(isset($no_agenda)?$no_agenda:auto_code('surat_keluar','',4)),'md-input','required');?>
		
		<!--endform-->
      </div>
       <!-- Modal footer -->
      <div class="modal-footer">
         <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</form>
</div><!--OPEN TABLE-->
<!-- page content -->

<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		DATA SURAT KELUAR</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">

					<!-- table -->
					<button type="button" class="btn btn-success btn-tambah" data-toggle="modal" data-target="#FormModal">
						<i class='fa fa-plus'></i> Tambah
					</button>					
					<hr>
					<form>
						<?php
							$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
							$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
						?>
						<div class="row">
							<div class="col-md-4">
								<?=select('bulan',bulanList(),$bulan)?>
							</div>
							<div class="col-md-4">
								<?=select('tahun',tahunList(),$tahun)?>
							</div>
							<div class="col-md-4">
								<button class="btn btn-info">Lihat</button>
								<a href="<?=site_url('cetak')?>?jenis=suratkeluar&bulan=<?=$bulan?>&tahun=<?=$tahun?>" class="btn btn-warning" target="_blank">Cetak</a>
							</div>
						</div>
					</form>
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
         
    </div>
</div>
<!-- /page content -->


