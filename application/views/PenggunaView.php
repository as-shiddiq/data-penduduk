	<!-- form -->
<!-- The Modal -->
<div class="modal fade" id="FormModal">
<form class="validate form-horizontal" id="form-pengguna" method="POST">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form pengguna</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <?=input_hidden('id_pengguna',(isset($id_pengguna)?$id_pengguna:''),'md-input','required');?>						
<div class="form-group">

<div class="col-md-3">
		<label>Nama Lengkap</label>
</div>

<div class="col-md-9">
		<?=input_text('nama_lengkap',(isset($nama_lengkap)?$nama_lengkap:''),'md-input','required');?>
</div>
</div>
						
<div class="form-group">

<div class="col-md-3">
		<label>Nama Pengguna</label>
</div>

<div class="col-md-9">
		<?=input_text('nama_pengguna',(isset($nama_pengguna)?$nama_pengguna:''),'md-input','required');?>
</div>
</div>
						
<div class="form-group">

<div class="col-md-3">
		<label>Kata Sandi</label>
</div>

<div class="col-md-9">
		<?=input_text('kata_sandi',(isset($kata_sandi)?$kata_sandi:''),'md-input','required');?>
</div>
</div>
						
<div class="form-group">

<div class="col-md-3">
		<label>Level</label>
</div>

<div class="col-md-9">
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$op['Administrator']='Administrator';
			$op['Petugas']='Petugas';
			echo select('level',$op,(isset($level)?$level:''),'','required');?>
</div>
</div>
<!--endform-->
      </div>
       <!-- Modal footer -->
      <div class="modal-footer">
         <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</form>
</div><!--OPEN TABLE-->
<!-- page content -->

<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		DATA PENGGUNA</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">

					<!-- table -->
					<button type="button" class="btn btn-success btn-tambah" data-toggle="modal" data-target="#FormModal">
						<i class='fa fa-plus'></i> Tambah
					</button>					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
         
    </div>
</div>
<!-- /page content -->


