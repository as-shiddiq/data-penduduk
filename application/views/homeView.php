<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
	<div class="col-md-12">
		<h1 class="weight-300" style="margin-bottom: 40px; margin-top: 60px">
		Selamat datang dihalaman <strong>Administrator</strong></h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="cre-animate" data-animation="scale-up" data-delay="500" data-easing="easeInOutBack" data-offset="80%" data-speed="1000">
			<div class="img-thumb" style="background-image: url('<?=base_url()?>publics/images/bg.jpeg');">
				<a >
				<div class="img-thumb-hover-left">
					<i class="im-redo2"></i></div>
				</a>
				<a data-pp="prettyPhoto[blog-gallery]" href="<?=base_url()?>publics/images/bg.jpeg" title="Take a Relaxing Vacation Adventure">
				<div class="img-thumb-hover-right">
					<i class="im-expand2"></i></div>
				</a>
				<img alt="blog-image" class="filler responsive" src="<?=base_url()?>publics/images/bg.jpeg">
			</div>
			<div class="blog-wrapper">
				<h3 style="padding:0;margin:0"><a >Kantor Desa Kandangan Lama</a></h3>
				<p>Jl. Pendidikan RT.06/RW.03 Kandangan Lama Kode Pos 70782</p>
			</div>
		</div>
	</div>
</div>