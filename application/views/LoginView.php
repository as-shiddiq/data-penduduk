<!DOCTYPE html>
<html>
<head>
    <?php include 'layouts/head.php';?>
	<title>Login</title>
	<style type="text/css">
		.container{
			/* font-size: 20px; */
			margin:5% auto;
			padding: 20px 5%
		}
		.btn{
			font-size: 18px
		}
		body{
			background-size: cover;
			background:url('<?=base_url()?>publics/images/bg.jpeg');
		}
		div.form{
			background: rgba(255,255,255,0.8);
			padding: 20px
		}
	</style>
</head>
<body>
<div class="container" >
	<div class="form">
		<?php echo $this->session->flashdata('info');?>

		<div class="row" style="padding-top:20px">

			<div class="col-md-5 text-center">
				<img src="<?=base_url('publics/images/logotala.png')?>" width="80%">
			</div>
			<div class="col-md-7">
				<form action="<?=site_url('login/check')?>" method="POST">
					<h2>Form Login</h2>
					<hr>
					<i>Masukan Nama Pengguna & Kata Sandi yang terdaftar</i>
					<div class="form-group">
						<label>Nama Pengguna</label>
						<input type="text" name="nama_pengguna" class="form-control">
					</div>
					<div class="form-group">
						<label>Kata Sandi</label>
						<input type="password" name="kata_sandi" class="form-control">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-info"><i class="fa fa-sign-in"></i> Login</button>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
</body>
      <?php include 'layouts/js.php'?>
</html>