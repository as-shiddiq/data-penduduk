<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
	include 'load/style.php';
	include 'load/kop.php';
	?>
	<h4 class="center"><u>LAPORAN BELUM E-KTP</u></h4>
<?php
    $tanggal='';
    $bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
    $tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
    $param=paramPendataan('');
    $template = array(
            'table_open' => '<table id="table">',
        );
    $this->table->set_template($template);
    $this->table->set_heading('No','Mempelai Pria','Mempelai Wanita','Penghulu','Lokasi Akad','tanggal');
    if($bulan!=''){
        $tanggal.='Bulan '.bulan_huruf($bulan);
    }
    if($tahun!=''){
        $tanggal.=' '.$tahun;
    }

    $i=1;
    $this->db->where_not_in('a.id_penduduk','SELECT id_penduduk FROM pendataan WHERE parameter="kematian"',false);
    $this->db->where_not_in('a.id_penduduk','SELECT id_penduduk FROM pendataan WHERE parameter="sudahektp"',false);
    $get_data=$this->pendudukModel->get_data();
    $this->table->set_heading('No','NIK','Nama','Tempat/Tanggal Lahir','Alamat','Jenis kelamin','Golongan Darah','Agama');
    foreach($get_data->result() as $row){
            $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                                            $row->nik,
                                                                            $row->nama,
                                                                            $row->tmpt_lhr.'/'.standar_tanggal($row->tgl_lhr),
                                                                            $row->alamat.' '.$row->nama_desa ,
                                                                            $row->jk,
                                                                            $row->goldar,
                                                                            $row->agama);
            $i++;
    }
    echo ($tanggal=='')?'':'Rekap Pendataan Penduduk : '.$tanggal.'<br>';
    echo $this->table->generate();
    ?>
    <small style="color:#999;font-size: 10px">
    generated at :<?=date('Y-m-d H:i:s')?>
    </small>
</body>
</html>