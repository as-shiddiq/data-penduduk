<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
	include 'load/style.php';
	include 'load/kop.php';
	?>
	<h4 class="center"><u>LAPORAN PENDATAAN PENDUDUK</u></h4>

	<?php
	$tanggal='';
	$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
	$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
	$param=paramPendataan('');
	$template = array(
            'table_open' => '<table id="table">',
        );
    $this->table->set_template($template);
	if($bulan!=''){
        $this->db->where("DATE_FORMAT(a.tgl_simpan,'%m')='".$bulan."'");
        $tanggal.='Bulan '.bulan_huruf($bulan);
    }
    if($tahun!=''){
        $this->db->where("DATE_FORMAT(a.tgl_simpan,'%Y')='".$tahun."'");
        $tanggal.=' '.$tahun;
    }

    $i=1;
	$this->db->where_not_in('a.id_penduduk','SELECT id_penduduk FROM pendataan WHERE parameter="kematian"',false);
    $this->db->where_not_in('a.id_penduduk','SELECT id_penduduk FROM pendataan WHERE parameter="pindahdomisili" AND pendataan="Keluar"',false);
	$get_data=$this->pendudukModel->get_data();
    $this->table->set_heading('No','NIK','Nama','Tempat/Tanggal Lahir','Alamt','Jenis kelamin','Golongan Darah','Agama','status','Hubungan Keluarga','Nama ibu','Nama ayah');
    foreach($get_data->result() as $row){
            $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                                            $row->nik,
                                                                            $row->nama,
                                                                            $row->tmpt_lhr.'/'.$row->tgl_lhr,
                                                                            $row->alamat.' '.$row->nama_desa ,
                                                                            $row->jk,
                                                                            $row->goldar,
                                                                            $row->agama,
                                                                            $row->status,
                                                                            $row->hub_kel,
                                                                            $row->nama_ibu,
                                                                            $row->nama_ayah);
            $i++;
    }
	echo ($tanggal=='')?'':'Rekap Pendataan Penduduk : '.$tanggal.'<br>';
    echo $this->table->generate();
	?>
	<small style="color:#999;font-size: 10px">
	generated at :<?=date('Y-m-d H:i:s')?>
	</small>
</body>
</html>