<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
	include 'load/style.php';
	include 'load/kop.php';
	?>
	<h4 class="center"><u>LAPORAN KEMATIAN PENDUDUK</u></h4>

	<?php
	$tanggal='';
	$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
	$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
	$param=paramPendataan($this->input->get('jenis'));
	$template = array(
            'table_open' => '<table id="table">',
        );
    $this->table->set_template($template);
    $this->table->set_heading('No','NIK','Nama','Tempat/Tanggal Lahir','Jenis Kelamin','Agama','Penyebab','Tempat Peristirahatan','Keterangan','Tanggal');
    $this->db->where($param['where']);
	if($bulan!=''){
        $this->db->where("DATE_FORMAT(tanggal,'%m')='".$bulan."'");
        $tanggal.=bulan_huruf($bulan);
    }
    if($tahun!=''){
        $this->db->where("DATE_FORMAT(tanggal,'%Y')='".$tahun."'");
        $tanggal.=' '.$tahun;
    }

    $this->db->where($param['where']);
    $i=1;
    $this->db->where($param['where']);
    $get_data=$this->pendudukModel->get_dataParamKematian();
    foreach($get_data->result() as $row){
        $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                    $row->nik,
                                                    $row->nama,
                                                    $row->tmpt_lhr.'/'.standar_tanggal($row->tgl_lhr),
                                                    $row->jk,
                                                    $row->agama,
                                                    $row->pendataan,
                                                    $row->keterangan,
                                                    $row->keterangan2,
                                                    standar_tanggal($row->tanggal));
        $i++;
    }
	echo 'Rekap : '.$tanggal.'<br>';
    echo $this->table->generate();
	?>
	<small style="color:#999;font-size: 10px">
	generated at :<?=date('Y-m-d H:i:s')?>
	</small>
</body>
</html>