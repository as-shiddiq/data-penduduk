<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
	include 'load/style.php';
	include 'load/kop.php';
	?>
	<h4 class="center"><u>LAPORAN SURAT KELUAR</u></h4>

	<?php
	$tanggal='';
	$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
	$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
	if($bulan!=''){
        $this->db->where("DATE_FORMAT(tanggal_surat,'%m')='".$bulan."'");
        $tanggal.=bulan_huruf($bulan);
    }
    if($tahun!=''){
        $this->db->where("DATE_FORMAT(tanggal_surat,'%Y')='".$tahun."'");
        $tanggal.=' '.$tahun;
    }

	$get_data=$this->suratkeluarModel->get_data();
	$template = array(
		'table_open' => '<table id="table">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','Nomor Surat','Tujuan','Perihal','Keterangan','Sifat','Tanggal Surat');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->no_surat,
													$row->tujuan,
													$row->perihal,
													$row->keterangan,
													$row->sifat,
													standar_tanggal($row->tanggal_surat));
		$i++;
	}
	echo 'Rekap : '.$tanggal.'<br>';
	echo $this->table->generate();
	?>
	<small style="color:#999;font-size: 10px">
	generated at :<?=date('Y-m-d H:i:s')?>
	</small>
</body>
</html>