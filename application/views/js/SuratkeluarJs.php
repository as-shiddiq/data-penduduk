
<!-- script form modal -->
<script>
    $(document).on("click",".btn-tambah",function(event){
        $('#form-suratkeluar').find("input,select,textarea").val("");
        var action="<?=site_url('suratkeluar/simpan')?>";
        $('#form-suratkeluar').attr({'action':action});
        
        $("#form-suratkeluar [name=no_agenda]").val('<?=auto_code('surat_keluar','',4)?>');
        $("#form-suratkeluar [name=no_surat]").val('/<?=auto_code('surat_keluar','',4).'/'.date('Y')?>');

    });
    $(document).on("click",".btn-sunting",function(event){
        event.preventDefault();
        var url=$(this).attr('href')+'&JSON';
        var action="<?=site_url('suratkeluar/ubah')?>";
        $("#FormModal").modal();
        $('#form-suratkeluar').attr({'action':action});
        $.getJSON( url, function( response ) {
            var data =response.data;
            $("#form-suratkeluar [name=id_surat_masuk]").val(data.id_surat_masuk);
            var data =response.data;
            $("#form-suratkeluar [name=no_surat]").val(data.no_surat);
            var data =response.data;
            $("#form-suratkeluar [name=tujuan]").val(data.tujuan);
            var data =response.data;
            $("#form-suratkeluar [name=perihal]").val(data.perihal);
            var data =response.data;
            $("#form-suratkeluar [name=keterangan]").val(data.keterangan);
            var data =response.data;
            $("#form-suratkeluar [name=sifat]").val(data.sifat);
            var data =response.data;
            $("#form-suratkeluar [name=tanggal_surat]").val(data.tanggal_surat);
            var data =response.data;
            $("#form-suratkeluar [name=no_agenda]").val(data.no_agenda);
        });
    })
</script>