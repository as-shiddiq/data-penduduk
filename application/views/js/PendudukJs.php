
<!-- script form modal -->
<script>
    getNik();
    $(".get-nik").change(getNik);
    function getNik(){
        var id_penduduk=$(".get-nik").val();
        if(id_penduduk!='' || id_penduduk!=undefined){
            console.log(id_penduduk);
            $(".load-nik").load("<?=site_url('ajax/getNik')?>/"+id_penduduk);
        }
    }

    getNik2();
    $(".get-nik2").change(getNik2);
    function getNik2(){
        var id_penduduk=$(".get-nik2").val();
        if(id_penduduk!='' || id_penduduk!=undefined){
            console.log(id_penduduk);
            $(".load-nik2").load("<?=site_url('ajax/getNik')?>/"+id_penduduk);
        }
    }

    hubKel();
    $("[name=hub_kel]").change(hubKel);
    function hubKel(){
        $val=$("[name=hub_kel").val();
        if($val!='Kepala Keluarga'){
            $(".hub-kel-dgn").show();
            $("[name=master_id]").attr({'required':'required'});
        }
        else{
            $(".hub-kel-dgn").hide();
            $("[name=master_id]").removeAttr('required');
        }
    }
    $(".statuspendataan").change(function(){
        $val=$(this).val();
        if($val=='Baru'){
            $(".load-penduduk").hide();
            $(".data-penduduk a").click();
            $(".form-penduduk input").attr('required','required');
            $(".form-penduduk select").attr('required','required');
            $(".form-penduduk textarea").attr('required','required');
            
            $(".form-status input").attr('disabled','disabled');
            $(".form-status select").attr('disabled','disabled');
            $(".form-status textarea").attr('disabled','disabled');
            $(".form-status .statuspendataan").removeAttr('disabled');

            alert('Anda hanya harus mengisi semua informasi dari penduduk');
        }
        else{
            $(".load-penduduk input").removeAttr('required');
            $(".form-penduduk select").removeAttr('required');
            $(".form-penduduk textarea").removeAttr('required');
            $(".form-status input").removeAttr('disabled');
            $(".form-status select").removeAttr('disabled');
            $(".form-status textarea").removeAttr('disabled');
            
            
            $(".load-penduduk").show();
        }
    })
    /*
    $(document).on("click",".btn-tambah",function(event){
        $('#form-penduduk').find("input,select,textarea").val("");
        var action="<?=site_url('penduduk/simpan')?>";
        $('#form-penduduk').attr({'action':action});
    });
    $(document).on("click",".btn-sunting",function(event){
        event.preventDefault();
        var url=$(this).attr('href')+'&JSON';
        var action="<?=site_url('penduduk/ubah')?>";
        $("#FormModal").modal();
        $('#form-penduduk').attr({'action':action});
        $.getJSON( url, function( response ) {
            var data =response.data;
            $("#form-penduduk [name=id_penduduk]").val(data.id_penduduk);
            $("#form-penduduk [name=id_pendidikan]").val(data.id_pendidikan);
            $("#form-penduduk [name=id_pekerjaan]").val(data.id_pekerjaan);
            $("#form-penduduk [name=nama]").val(data.nama);
            $("#form-penduduk [name=nik]").val(data.nik);
            $("#form-penduduk [name=jk]").val(data.jk);
            $("#form-penduduk [name=tmpt_lhr]").val(data.tmpt_lhr);
            $("#form-penduduk [name=tgl_lhr]").val(data.tgl_lhr);
            $("#form-penduduk [name=goldar]").val(data.goldar);
            $("#form-penduduk [name=agama]").val(data.agama);
            $("#form-penduduk [name=status]").val(data.status);
            $("#form-penduduk [name=hub_kel]").val(data.hub_kel);
            $("#form-penduduk [name=nama_ibu]").val(data.nama_ibu);
            $("#form-penduduk [name=nama_ayah]").val(data.nama_ayah);
            $("#form-penduduk [name=keterangan]").val(data.keterangan);
            $("#form-penduduk [name=master_id]").val(data.master_id);
        });
    })
    */
</script>