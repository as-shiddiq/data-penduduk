
<!-- script form modal -->
<script>
    $(document).on("click",".btn-tambah",function(event){
        $('#form-bidangusaha').find("input,select,textarea").val("");
        var action="<?=site_url($config['url'].'/simpan')?>";
        $('#form-bidangusaha').attr({'action':action});
    });
    $(document).on("click",".btn-sunting",function(event){
        event.preventDefault();
        var url=$(this).attr('href')+'&JSON';
        var action="<?=site_url($config['url'].'/ubah')?>";
        $("#FormModal").modal();
        $('#form-bidangusaha').attr({'action':action});
        $.getJSON( url, function( response ) {
            var data =response.data;
            // disamakan dengan field di database
            $("#form-bidangusaha [name=id_bidang_usaha]").val(data.id_bidang_usaha);
            $("#form-bidangusaha [name=bidang_usaha]").val(data.bidang_usaha);
        });
    })
</script>