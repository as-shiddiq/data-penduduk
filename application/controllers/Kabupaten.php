<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('kabupatenModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->kabupatenModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama provinsi','nama kabupaten','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_provinsi,
													$row->nama_kabupaten,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("kabupaten/sunting?id_kabupaten=".$row->id_kabupaten),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("kabupaten/hapus?id_kabupaten=".$row->id_kabupaten),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='kabupaten';
	$data['body']=$this->load->view('kabupatenView',$databody,true);
	$data['js']=$this->load->view('js/kabupatenJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_kabupaten'=>$_GET['id_kabupaten']
			];
	$row=$this->db->get_where('kabupaten',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->kabupatenModel->insert($data);
	}
	redirect('kabupaten');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_kabupaten=$this->input->post('id_kabupaten');
		$where=array(
			'id_kabupaten'=>$id_kabupaten
		);
		$this->kabupatenModel->update($data,$where);
	}
	redirect('Kabupaten');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_kabupaten'=>$this->input->get(id_kabupaten)
		);
	$this->kabupatenModel->delete($where);
	redirect('kabupaten');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_kabupaten = $this->input->post('id_kabupaten');
	$id_provinsi = $this->input->post('id_provinsi');
	$nama_kabupaten = $this->input->post('nama_kabupaten');
	$data=array(
			'id_kabupaten'=>$id_kabupaten,
			'id_provinsi'=>$id_provinsi,
			'nama_kabupaten'=>$nama_kabupaten
		);
	return $data;
}
//end class
}
