<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Desa extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('desaModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->desaModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama kecamatan','nama desa','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_kecamatan,
													$row->nama_desa,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("desa/sunting?id_desa=".$row->id_desa),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("desa/hapus?id_desa=".$row->id_desa),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='desa';
	$data['body']=$this->load->view('desaView',$databody,true);
	$data['js']=$this->load->view('js/desaJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_desa'=>$_GET['id_desa']
			];
	$row=$this->db->get_where('desa',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->desaModel->insert($data);
	}
	redirect('desa');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_desa=$this->input->post('id_desa');
		$where=array(
			'id_desa'=>$id_desa
		);
		$this->desaModel->update($data,$where);
	}
	redirect('Desa');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_desa'=>$this->input->get(id_desa)
		);
	$this->desaModel->delete($where);
	redirect('desa');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_desa = $this->input->post('id_desa');
	$id_kecamatan = $this->input->post('id_kecamatan');
	$nama_desa = $this->input->post('nama_desa');
	$data=array(
			'id_desa'=>$id_desa,
			'id_kecamatan'=>$id_kecamatan,
			'nama_desa'=>$nama_desa
		);
	return $data;
}
//end class
}
