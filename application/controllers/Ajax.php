<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function getKabupaten($id='',$load=''){
		?>
		<?php if ($load!='true'): ?>
		<div class="col-md-3">
			<label>Kabupaten/Kota</label>				
		</div>
		<div class="col-md-9">
		<?php endif ?>
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					if($id!=''){
						$this->db->order_by('nama_kabupaten','ASC');
						$this->db->where('id_provinsi',$id);
						$data=$this->db->select('*')->get('kabupaten');
						foreach($data->result() as $row){
							$op[$row->id_kabupaten]=$row->nama_kabupaten;
						}
					}
					if($load=='true'){
						echo select('id_kabupaten',$op,'','get-kecamatan',' data-select="true"');
					}
					else{
						echo select('id_kabupaten',$op,'','get-kecamatan','');
					}
				?>
		<script type="text/javascript">
		$("select").select2();
		<?php if ($load!='true'): ?>
		</script>
		</div>
		<?php endif ?>
		<?php
	}
	function getKecamatan($id='',$load=''){
		?>
		<?php if ($load!='true'): ?>
		<div class="col-md-3">
			<label>Kecamatan</label>				
		</div>
		<div class="col-md-9">
		<?php endif ?>
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					if($id!=''){
						$this->db->order_by('nama_kecamatan','ASC');
						$this->db->where('id_kabupaten',$id);
						$data=$this->db->select('*')->get('kecamatan');
						foreach($data->result() as $row){
							$op[$row->id_kecamatan]=$row->nama_kecamatan;
						}
					}
					if($load=='true'){
						echo select('id_kecamatan',$op,'','get-desa',' data-select="true"');
					}
					else{
						echo select('id_kecamatan',$op,'','get-desa','');
					}
				?>
		<script type="text/javascript">
		$("select").select2();
		</script>
		<?php if ($load!='true'): ?>
		</div>
		<?php endif ?>
		<?php
	}

	function getDesa($id='',$load=''){
		?>
		<?php if ($load!='true'): ?>
		<div class="col-md-3">
			<label>Kelurahan/Desa</label>				
		</div>
		<?php endif ?>
		<div class="col-md-9">
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					if($id!=''){
						$this->db->order_by('kelurahan','DESC');
						$this->db->order_by('nama_desa','ASC');
						$this->db->where('id_kecamatan',$id);
						$data=$this->db->select('*')->get('desa');
						foreach($data->result() as $row){
							$op[$row->id_desa]=$row->nama_desa;
						}
					}
					if($load=='true'){
						echo select('id_desa',$op,'','',' data-select="true"');
					}
					else{
						echo select('id_desa',$op,'','','');
					}
				?>
		<script type="text/javascript">
		$("select").select2();
		</script>
		<?php if ($load!='true'): ?>
		</div>
		<?php endif ?>
		<?php
	}
	function getNik($id_penduduk=''){
		$this->load->model('PendudukModel');
		$this->db->where('a.id_penduduk',$id_penduduk);
		$get=$this->PendudukModel->get_data();
		if($get->num_rows()>0){
			$row=$get->row();
			$this->table->add_row('NIK',':',$row->nik);
			$this->table->add_row('Nama',':',$row->nama);
			$this->table->add_row('Jenis Kelamin',':',$row->jk);
			$this->table->add_row('Golongan Darah',':',$row->goldar);
			$this->table->add_row('Tempat/Tanggal Lahir',':',$row->tmpt_lhr.'/'.standar_tanggal($row->tgl_lhr));
			$this->table->add_row('Agama',':',$row->agama);
			$this->table->add_row('Alamat',':',$row->alamat.' Ds.'.$row->nama_desa.', Kec. '.$row->nama_kecamatan.', Kab. '.$row->nama_kabupaten);
			$this->table->add_row('Pendidikan',':',$row->nama_pendidikan);
			$this->table->add_row('Pekerjaan',':',$row->nama_pekerjaan);
			if($row->hub_kel=='Kepala Keluarga'){
				$this->table->add_row('Hubungan Keluarga',':',$row->hub_kel);
			}
			else{
				$this->table->add_row('Hubungan Keluarga',':',$row->hub_kel.' ('.$row->nama_hubungan.')');
			}
			$this->table->add_row('Nama Ayam',':',$row->nama_ayah);
			$this->table->add_row('Nama Ibu',':',$row->nama_ibu);

			echo $this->table->generate();
		}
		else{
			echo info_warning('Maaf data penduduk tidak ditemukan');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
