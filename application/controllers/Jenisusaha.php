<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenisusaha extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('jenisusahaModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->jenisusahaModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','jenis usaha','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->jenis_usaha,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("jenisusaha/sunting?id_jenis_usaha=".$row->id_jenis_usaha),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("jenisusaha/hapus?id_jenis_usaha=".$row->id_jenis_usaha),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Jenis Usaha';
	$data['body']=$this->load->view('jenisusahaView',$databody,true);
	$data['js']=$this->load->view('js/jenisusahaJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_jenis_usaha'=>$_GET['id_jenis_usaha']
			];
	$row=$this->db->get_where('jenis_usaha',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->jenisusahaModel->insert($data);
	}
	redirect('jenisusaha');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_jenis_usaha=$this->input->post('id_jenis_usaha');
		$where=array(
			'id_jenis_usaha'=>$id_jenis_usaha
		);
		$this->jenisusahaModel->update($data,$where);
	}
	redirect('Jenisusaha');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_jenis_usaha'=>$this->input->get(id_jenis_usaha)
		);
	$this->jenisusahaModel->delete($where);
	redirect('jenisusaha');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_jenis_usaha = $this->input->post('id_jenis_usaha');
	$jenis_usaha = $this->input->post('jenis_usaha');
	$data=array(
			'id_jenis_usaha'=>$id_jenis_usaha,
			'jenis_usaha'=>$jenis_usaha
		);
	return $data;
}
//end class
}
