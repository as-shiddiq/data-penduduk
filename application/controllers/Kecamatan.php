<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('kecamatanModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->kecamatanModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama kabupaten','nama kecamatan','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_kabupaten,
													$row->nama_kecamatan,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("kecamatan/sunting?id_kecamatan=".$row->id_kecamatan),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("kecamatan/hapus?id_kecamatan=".$row->id_kecamatan),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='kecamatan';
	$data['body']=$this->load->view('kecamatanView',$databody,true);
	$data['js']=$this->load->view('js/kecamatanJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_kecamatan'=>$_GET['id_kecamatan']
			];
	$row=$this->db->get_where('kecamatan',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->kecamatanModel->insert($data);
	}
	redirect('kecamatan');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_kecamatan=$this->input->post('id_kecamatan');
		$where=array(
			'id_kecamatan'=>$id_kecamatan
		);
		$this->kecamatanModel->update($data,$where);
	}
	redirect('Kecamatan');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_kecamatan'=>$this->input->get(id_kecamatan)
		);
	$this->kecamatanModel->delete($where);
	redirect('kecamatan');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_kecamatan = $this->input->post('id_kecamatan');
	$id_kabupaten = $this->input->post('id_kabupaten');
	$nama_kecamatan = $this->input->post('nama_kecamatan');
	$data=array(
			'id_kecamatan'=>$id_kecamatan,
			'id_kabupaten'=>$id_kabupaten,
			'nama_kecamatan'=>$nama_kecamatan
		);
	return $data;
}
//end class
}
