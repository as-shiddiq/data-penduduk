<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siswa extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
}
public function _config(){
	$data['url']="siswa";
	$data['table']='siswa';
	$data['primaryKey']='id_siswa';
	$data['fillable']=['nama_siswa','alamat'];
	return $data;
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$config=$this->_config();
	$get_data=$this->db->get($config['table']);
	$primaryKey=$config['primaryKey'];
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','Nama Siswa','ALamat','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_siswa,
													$row->alamat,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url($config['url']."/sunting?".$config['primaryKey']."=".$row->$primaryKey),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url($config['url']."/hapus?".$config['primaryKey']."=".$row->$primaryKey),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['config']=$config;
	$databody['table']=$this->table->generate();
	$data['title']='Bidang Usaha';
	$data['body']=$this->load->view('siswaView',$databody,true);
	$data['js']=$this->load->view('js/siswaJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$config=$this->_config();
	
	$where=[
			$config['primaryKey']=>$_GET[$config['primaryKey']]
			];
	$row=$this->db->get_where($config['table'],$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	$config=$this->_config();
	if($this->input->post('simpan')){
        $data= request_all($config['fillable']);
		$this->db->insert($config['table'],$data);
	}
	redirect($config['url']);
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$config=$this->_config();
		$data= request_all($config['fillable']);
		$where=array(
			$config['primaryKey']=>$this->input->post($config['primaryKey'])
		);
		$this->db->update($config['table'],$data,$where);
	}
	redirect($config['url']);
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$config=$this->_config();
	
		$where=array(
			$config['primaryKey']=>$this->input->get($config['primaryKey'])
		);
	$this->db->delete($config['table'],$where);
	redirect($config['url']);
}
##################################
##      KUMPULKAN NILAI POST    ##
############
}
