<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penduduk extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('pendudukModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index($param=''){
	$param=paramPendataan($param);

	$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
	$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
	if($bulan!=''){
        $this->db->where("DATE_FORMAT(a.tgl_simpan,'%m')='".$bulan."'");
    }
    if($tahun!=''){
        $this->db->where("DATE_FORMAT(a.tgl_simpan,'%Y')='".$tahun."'");
    }
	$this->db->where_not_in('a.id_penduduk','SELECT id_penduduk FROM pendataan WHERE parameter="kematian"',false);
	$this->db->where_not_in('a.id_penduduk','SELECT id_penduduk FROM pendataan WHERE parameter="pindahdomisili" AND pendataan="Keluar"',false);
	$get_data=$this->pendudukModel->get_data();
	$databody['get_data']=$get_data;
	$databody['param']=$param;
	$databody['msg']=($param['param']==null)?'':info_warning($param['msg']);
	$databody['title']=($param['param']==null)?'DATA PENDUDUK':strtoupper($param['msg']);
	$data['title']='Penduduk';
	$data['body']=$this->load->view('pendudukView',$databody,true);
	$data['js']=$this->load->view('js/pendudukJs',$databody,true);
	$this->load->view('layouts/html',$data);
}

##################################
##       CATCH PENDUDUK         ##
##################################

public function _catch(){
	$where=[
			'id_penduduk'=>$_GET['id_penduduk']
			];
	$row=$this->db->get_where('penduduk',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan($param=''){
	if($param!=''){
		$param=paramPendataan($param);
		if($this->input->post('simpan')){
			$data2=$this->_datapost2();
			if($this->input->post('status_pendataan')=='Baru' OR $this->input->post('id_penduduk')==''){
				$data=$this->_datapost();
				$this->pendudukModel->insert($data);
				$id_penduduk=$this->db->order_by('id_penduduk','DESC')->get('penduduk')->row()->id_penduduk;
				$data2['id_penduduk']=$id_penduduk;
			}
			$this->pendudukModel->insertPendataan($data2);
		}	
		redirect('penduduk/index/'.$this->input->post('parameter'));
	}
	else{
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->db->where(['nik'=>$_POST['nik']]);
			$this->db->where(['nik !='=>'']);
			$cek=$this->db->get('penduduk');
			if($cek->num_rows()==0){
				$this->pendudukModel->insert($data);
			}
		}
		redirect('penduduk');
	}
}
##################################
##            UBAH              ##
##################################

public function ubah($param=''){

	if($param!=''){
		if($this->input->post('simpan')){
			$data=$this->_datapost2();
			$id_pendataan=$this->input->post('id_pendataan');
			$where=array(
				'id_pendataan'=>$id_pendataan
			);
			$this->pendudukModel->updatePendataan($data,$where);
		}	
		redirect('penduduk/index/'.$this->input->post('parameter'));
	}
	else{
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_penduduk=$this->input->post('id_penduduk');
			$where=array(
				'id_penduduk'=>$id_penduduk
			);
			$this->pendudukModel->update($data,$where);
		}
		redirect('penduduk');
	}
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_penduduk'=>$this->input->get('id_penduduk')
		);
	$this->pendudukModel->delete($where);
	redirect('penduduk');
}
public function hapuspendataan($param=''){
	$where=array(
			'id_pendataan'=>$this->input->get('id_pendataan')
		);
	$this->db->delete('pendataan',$where);
	redirect('penduduk/index/'.$param);
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$data=null;
	$id_penduduk = $this->input->post('id_penduduk');
	$nik = $this->input->post('nik');
	$id_pendidikan = $this->input->post('id_pendidikan');
	$id_pekerjaan = $this->input->post('id_pekerjaan');
	$id_desa = $this->input->post('id_desa');
	$nama = $this->input->post('nama');
	$alamat = $this->input->post('alamat');
	$jk = $this->input->post('jk');
	$tmpt_lhr = $this->input->post('tmpt_lhr');
	$tgl_lhr = $this->input->post('tgl_lhr');
	$goldar = $this->input->post('goldar');
	$agama = $this->input->post('agama');
	$status = $this->input->post('status');
	$hub_kel = $this->input->post('hub_kel');
	$nama_ibu = $this->input->post('nama_ibu');
	$nama_ayah = $this->input->post('nama_ayah');
	$keterangan = $this->input->post('keterangan');
	$keterangan2 = $this->input->post('keterangan2');
	$keterangan3 = $this->input->post('keterangan3');
	$master_id = $this->input->post('master_id');
	$data=array(
			'id_penduduk'=>$id_penduduk,
			'id_pendidikan'=>$id_pendidikan,
			'id_pekerjaan'=>$id_pekerjaan,
			'id_desa'=>$id_desa,
			'nik'=>$nik,
			'nama'=>$nama,
			'jk'=>$jk,
			'alamat'=>$alamat,
			'tmpt_lhr'=>$tmpt_lhr,
			'tgl_lhr'=>$tgl_lhr,
			'goldar'=>$goldar,
			'agama'=>$agama,
			'status'=>$status,
			'hub_kel'=>$hub_kel,
			'nama_ibu'=>$nama_ibu,
			'nama_ayah'=>$nama_ayah,
			'keterangan'=>$keterangan,
			'master_id'=>$master_id,
			'tgl_simpan'=>date('Y-m-d H:i:s')
		);
	return $data;
}
private function _datapost2(){
	$data=null;
	$id_pendataan = $this->input->post('id_pendataan');
	$id_penduduk = $this->input->post('id_penduduk');
	$pendataan = $this->input->post('pendataan');
	$parameter = $this->input->post('parameter');
	$keterangan = $this->input->post('keterangan');
	$keterangan2 = $this->input->post('keterangan2');
	$keterangan3 = $this->input->post('keterangan3');
	$tanggal = $this->input->post('tanggal');
	$id1 = $this->input->post('id1');
	$id2 = $this->input->post('id2');
	$status_pendataan = $this->input->post('status_pendataan');
	$data=array(
			'id_pendataan'=>$id_pendataan,
			'id_penduduk'=>$id_penduduk,
			'pendataan'=>$pendataan,
			'parameter'=>$parameter,
			'keterangan'=>$keterangan,
			'keterangan2'=>$keterangan2,
			'keterangan3'=>$keterangan3,
			'tanggal'=>$tanggal,
			'id1'=>$id1,
			'id2'=>$id2,
			'status_pendataan'=>$status_pendataan
		);
	return $data;
}
//end class
}
