<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pekerjaan extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('pekerjaanModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->pekerjaanModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama pekerjaan','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_pekerjaan,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("pekerjaan/sunting?id_pekerjaan=".$row->id_pekerjaan),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("pekerjaan/hapus?id_pekerjaan=".$row->id_pekerjaan),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='pekerjaan';
	$data['body']=$this->load->view('pekerjaanView',$databody,true);
	$data['js']=$this->load->view('js/pekerjaanJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_pekerjaan'=>$_GET['id_pekerjaan']
			];
	$row=$this->db->get_where('pekerjaan',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->pekerjaanModel->insert($data);
	}
	redirect('pekerjaan');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_pekerjaan=$this->input->post('id_pekerjaan');
		$where=array(
			'id_pekerjaan'=>$id_pekerjaan
		);
		$this->pekerjaanModel->update($data,$where);
	}
	redirect('Pekerjaan');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_pekerjaan'=>$this->input->get(id_pekerjaan)
		);
	$this->pekerjaanModel->delete($where);
	redirect('pekerjaan');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_pekerjaan = $this->input->post('id_pekerjaan');
	$nama_pekerjaan = $this->input->post('nama_pekerjaan');
	$data=array(
			'id_pekerjaan'=>$id_pekerjaan,
			'nama_pekerjaan'=>$nama_pekerjaan
		);
	return $data;
}
//end class
}
