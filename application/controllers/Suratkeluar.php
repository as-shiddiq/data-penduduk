<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suratkeluar extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('suratkeluarModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){

	$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
	$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
	if($bulan!=''){
        $this->db->where("DATE_FORMAT(tanggal_surat,'%m')='".$bulan."'");
    }
    if($tahun!=''){
        $this->db->where("DATE_FORMAT(tanggal_surat,'%Y')='".$tahun."'");
    }

	$get_data=$this->suratkeluarModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','Nomor Surat','Tujuan','Perihal','Keterangan','Sifat','Tanggal Surat','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->no_surat,
													$row->tujuan,
													$row->perihal,
													$row->keterangan,
													$row->sifat,
													standar_tanggal($row->tanggal_surat),
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("suratkeluar/sunting?id_surat_masuk=".$row->id_surat_masuk),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("suratkeluar/hapus?id_surat_masuk=".$row->id_surat_masuk),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Surat Keluar';
	$data['body']=$this->load->view('suratkeluarView',$databody,true);
	$data['js']=$this->load->view('js/suratkeluarJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_surat_masuk'=>$_GET['id_surat_masuk']
			];
	$row=$this->db->get_where('surat_keluar',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->suratkeluarModel->insert($data);
	}
	redirect('suratkeluar');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_surat_masuk=$this->input->post('id_surat_masuk');
		$where=array(
			'id_surat_masuk'=>$id_surat_masuk
		);
		$this->suratkeluarModel->update($data,$where);
	}
	redirect('Suratkeluar');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_surat_masuk'=>$this->input->get(id_surat_masuk)
		);
	$this->suratkeluarModel->delete($where);
	redirect('suratkeluar');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_surat_masuk = $this->input->post('id_surat_masuk');
	$no_surat = $this->input->post('no_surat');
	$tujuan = $this->input->post('tujuan');
	$perihal = $this->input->post('perihal');
	$keterangan = $this->input->post('keterangan');
	$sifat = $this->input->post('sifat');
	$tanggal_surat = $this->input->post('tanggal_surat');
	$no_agenda = $this->input->post('no_agenda');
	$data=array(
			'id_surat_masuk'=>$id_surat_masuk,
			'no_surat'=>$no_surat,
			'tujuan'=>$tujuan,
			'perihal'=>$perihal,
			'keterangan'=>$keterangan,
			'sifat'=>$sifat,
			'tanggal_surat'=>$tanggal_surat,
			'no_agenda'=>$no_agenda
		);
	return $data;
}
//end class
}
