<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinsi extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('provinsiModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->provinsiModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama provinsi','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_provinsi,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("provinsi/sunting?id_provinsi=".$row->id_provinsi),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("provinsi/hapus?id_provinsi=".$row->id_provinsi),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Provinsi';
	$data['body']=$this->load->view('provinsiView',$databody,true);
	$data['js']=$this->load->view('js/provinsiJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_provinsi'=>$_GET['id_provinsi']
			];
	$row=$this->db->get_where('provinsi',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->provinsiModel->insert($data);
	}
	redirect('provinsi');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_provinsi=$this->input->post('id_provinsi');
		$where=array(
			'id_provinsi'=>$id_provinsi
		);
		$this->provinsiModel->update($data,$where);
	}
	redirect('Provinsi');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_provinsi'=>$this->input->get(id_provinsi)
		);
	$this->provinsiModel->delete($where);
	redirect('provinsi');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_provinsi = $this->input->post('id_provinsi');
	$nama_provinsi = $this->input->post('nama_provinsi');
	$data=array(
			'id_provinsi'=>$id_provinsi,
			'nama_provinsi'=>$nama_provinsi
		);
	return $data;
}
//end class
}
