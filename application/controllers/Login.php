<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
   {
	        parent::__construct();
			$this->load->model(['penggunaModel']);
   }

	public function index($a='')
	{
		$data['title']='Daftar';
		$this->load->view('LoginView',$data);
	}
	
	public function check($a="",$b="")
	{
		$nama_pengguna=$this->input->post('nama_pengguna');
		$kata_sandi=$this->input->post('kata_sandi');
		//cek user
		$this->db->where(['nama_pengguna'=>$nama_pengguna,'kata_sandi'=>$kata_sandi]);
		$cek=$this->penggunaModel->get_data();
		if($cek->num_rows()==1){
			$row=$cek->row_array();
			$this->session->set_userdata('logged','logged');
			$this->session->set_userdata($row);
			$this->session->set_flashdata('info',info_success(icon('smile-o').' Selamat Datang <b>'.$row['surel'].'</b>'));
			redirect('home');
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Data Akun tidak dapat ditemukan di dalam basisdata'));
			redirect('login');
		}

	}
	function logout(){
		$this->session->sess_destroy();
		if(isset($_GET['to'])){
			redirect($_GET['to']);
		}
		else{
			redirect('login');
		}
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
