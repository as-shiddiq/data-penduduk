<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengguna extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('penggunaModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->penggunaModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama pengguna','kata sandi','nama lengkap','level','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_pengguna,
													$row->kata_sandi,
													$row->nama_lengkap,
													$row->level,
													array("data"=>'<button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><i class="fa fa-gears"></i> Aksi <span class="caret"></span></button><ul class="dropdown-menu">
                                                    <li>'.anchor(site_url("pengguna/sunting?id_pengguna=".$row->id_pengguna),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn-sunting","onclick"=>false]).'</li>
                                                    <li>'.anchor(site_url("pengguna/hapus?id_pengguna=".$row->id_pengguna),"<i class='fa fa-trash-o'></i> Hapus",["onclick"=>"return confirm('Yakin Hapus Data?')"]).'</li>
                                                    </ul>',"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='pengguna';
	$data['body']=$this->load->view('penggunaView',$databody,true);
	$data['js']=$this->load->view('js/penggunaJs',$databody,true);
	$this->load->view('layouts/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_pengguna'=>$_GET['id_pengguna']
			];
	$row=$this->db->get_where('pengguna',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->penggunaModel->insert($data);
	}
	redirect('pengguna');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_pengguna=$this->input->post('id_pengguna');
		$where=array(
			'id_pengguna'=>$id_pengguna
		);
		$this->penggunaModel->update($data,$where);
	}
	redirect('Pengguna');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_pengguna'=>$this->input->get(id_pengguna)
		);
	$this->penggunaModel->delete($where);
	redirect('pengguna');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_pengguna = $this->input->post('id_pengguna');
	$nama_pengguna = $this->input->post('nama_pengguna');
	$kata_sandi = $this->input->post('kata_sandi');
	$nama_lengkap = $this->input->post('nama_lengkap');
	$level = $this->input->post('level');
	$data=array(
			'id_pengguna'=>$id_pengguna,
			'nama_pengguna'=>$nama_pengguna,
			'kata_sandi'=>$kata_sandi,
			'nama_lengkap'=>$nama_lengkap,
			'level'=>$level
		);
	return $data;
}
//end class
}
