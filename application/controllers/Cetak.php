<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('desaModel');
	$this->load->model('suratkeluarModel');
	$this->load->model('pendudukModel');

}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
		$jenis=$this->input->get('jenis');
		$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
		$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');

		switch ($jenis) {
			case 'suratkeluar':
				break;
			
			default:
				# code...
				break;
		}
		$pdf=$this->load->view('cetak/'.$jenis,'',TRUE);
		$namafile=$jenis.$bulan.$tahun;

		// echo $pdf;
		generate_pdf($pdf,$namafile,'legal','landscape');
	}
}
