<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProvinsiModel extends CI_Model {
function get_data(){
  $data=$this->db->select('*')
              ->from('provinsi')
              ->order_by('id_provinsi','ASC')
              ->get();
  return $data;
}

function insert($data){
  $this->db->insert('provinsi',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('provinsi',$where);
  if($cek->num_rows()>0){
    $this->db->update('provinsi',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('provinsi',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
