<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DesaModel extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.nama_kecamatan')
        ->from('desa a')
        ->join('kecamatan b','a.id_kecamatan=b.id_kecamatan','left')
        ->order_by('id_desa','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('desa',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('desa',$where);
  if($cek->num_rows()>0){
    $this->db->update('desa',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('desa',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
