<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PendudukModel extends CI_Model {
function get_data(){
  // $data=$this->db->select('a.*,b.nama_pendidikan,c.nama_pekerjaan,a.nama as nama_penduduk,d.nama as nama_hubungan,e.*,f.*,g.*,h.*')
  $data=$this->db->select('a.*,b.nama_pendidikan,c.nama_pekerjaan,a.nama as nama_penduduk,d.nama as nama_hubungan')
        ->from('penduduk a')
        ->join('pendidikan b','a.id_pendidikan=b.id_pendidikan','left')
				->join('pekerjaan c','a.id_pekerjaan=c.id_pekerjaan','left')
				->join('penduduk d','a.id_penduduk=d.id_penduduk','left')
				// ->join('desa e','a.id_desa=e.id_desa','left')
				// ->join('kecamatan f','e.id_kecamatan=f.id_kecamatan','left')
				// ->join('kabupaten g','f.id_kabupaten=g.id_kabupaten','left')
				// ->join('provinsi h','g.id_provinsi=h.id_provinsi','left')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}
function get_dataParam(){
  $data=$this->db->select('a.*,b.nama_pendidikan,c.nama_pekerjaan,d.nama,e.*')
        ->from('penduduk a')
        ->join('pendidikan b','a.id_pendidikan=b.id_pendidikan','left')
        ->join('pekerjaan c','a.id_pekerjaan=c.id_pekerjaan','left')
        ->join('penduduk d','a.id_penduduk=d.id_penduduk','left')
        ->join('pendataan e','a.id_penduduk=e.id_penduduk','INNER')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}

function get_dataParamStatus(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}

function get_dataParamKelahiran(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}
function get_dataParamKematian(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}

function get_dataParamKurangMampu(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan,c.nama_pekerjaan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->join('pekerjaan c','b.id_pekerjaan=c.id_pekerjaan','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}

function get_dataParamSudahEktp(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}
function get_dataParamIjinUsaha(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan,c.bidang_usaha')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->join('bidang_usaha c','a.id1=c.id_bidang_usaha','LEFT')
        // ->join('desa d','a.id2=d.id_desa','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}
function get_dataParamPindahDomisili(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        // ->join('desa c','a.id1=c.id_desa','LEFT')
        // ->join('desa d','a.id2=d.id_desa','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}

function get_dataParamMenikah(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan,c.nama as pasangan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->join('penduduk c','a.id1=c.id_penduduk','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}


function get_dataParamCerai(){
  $data=$this->db->select('a.*,b.*,b.keterangan as ket_pen,a.keterangan as keterangan,c.nama as pasangan')
        ->from('pendataan a')
        ->join('penduduk b','a.id_penduduk=b.id_penduduk','LEFT')
        ->join('penduduk c','a.id1=c.id_penduduk','LEFT')
        ->order_by('a.id_penduduk','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('penduduk',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function insertPendataan($data){
  $this->db->insert('pendataan',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('penduduk',$where);
  if($cek->num_rows()>0){
    $this->db->update('penduduk',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function updatePendataan($data,$where){
  $cek=$this->db->get_where('pendataan',$where);
  if($cek->num_rows()>0){
    $this->db->update('pendataan',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('penduduk',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
