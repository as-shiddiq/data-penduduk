<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KabupatenModel extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.nama_provinsi')
        ->from('kabupaten a')
        ->join('provinsi b','a.id_provinsi=b.id_provinsi','left')
        ->order_by('id_kabupaten','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('kabupaten',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('kabupaten',$where);
  if($cek->num_rows()>0){
    $this->db->update('kabupaten',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('kabupaten',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
