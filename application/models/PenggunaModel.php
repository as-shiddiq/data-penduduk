<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PenggunaModel extends CI_Model {
function get_data(){
  $data=$this->db->select('*')
              ->from('pengguna')
              ->order_by('id_pengguna','ASC')
              ->get();
  return $data;
}

function insert($data){
  $this->db->insert('pengguna',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('pengguna',$where);
  if($cek->num_rows()>0){
    $this->db->update('pengguna',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('pengguna',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
