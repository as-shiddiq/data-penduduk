<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KecamatanModel extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.nama_kabupaten')
        ->from('kecamatan a')
        ->join('kabupaten b','a.id_kabupaten=b.id_kabupaten','left')
        ->order_by('id_kecamatan','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('kecamatan',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('kecamatan',$where);
  if($cek->num_rows()>0){
    $this->db->update('kecamatan',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('kecamatan',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
