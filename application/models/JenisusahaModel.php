<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class JenisusahaModel extends CI_Model {
function get_data(){
  $data=$this->db->select('*')
              ->from('jenis_usaha')
              ->order_by('id_jenis_usaha','ASC')
              ->get();
  return $data;
}

function insert($data){
  $this->db->insert('jenis_usaha',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('jenis_usaha',$where);
  if($cek->num_rows()>0){
    $this->db->update('jenis_usaha',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('jenis_usaha',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
