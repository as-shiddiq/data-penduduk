<?php
    function templates($a=''){
        return base_url().'publics/Haze/'.$a;
    }

    function paramPendataan($a){
        switch ($a) {
            case 'status':
                return ['param'=>'status',
                            'include'=>'status.php',
                            'msg'=>'Pendataan Status Penduduk',
                            'where'=>['parameter'=>'status']
                        ];
                break;
            case 'kelahiran':
                return ['param'=>'kelahiran',
                            'include'=>'kelahiran.php',
                            'msg'=>'Pendataan Kelahiran Penduduk',
                            'where'=>['parameter'=>'kelahiran']
                        ];
                break;
            
            case 'kematian':
                return ['param'=>'kematian',
                            'include'=>'kematian.php',
                            'msg'=>'Pendataan Kematian Penduduk',
                            'where'=>['parameter'=>'kematian']
                        ];
                break;
            
            case 'kurangmampu':
                return ['param'=>'kurangmampu',
                            'include'=>'kurangmampu.php',
                            'msg'=>'Pendataan Penduduk Kurang Mampu',
                            'where'=>['parameter'=>'kurangmampu']
                        ];
                break;
            
            case 'sudahektp':
                return ['param'=>'sudahektp',
                            'include'=>'sudahektp.php',
                            'msg'=>'Pendataan Status E-KTP',
                            'where'=>['parameter'=>'sudahektp']
                        ];
                break;
            
            case 'ijinusaha':
                return ['param'=>'ijinusaha',
                            'include'=>'ijinusaha.php',
                            'msg'=>'Pendataan Ijin Usaha Penduduk',
                            'where'=>['parameter'=>'ijinusaha']
                        ];
                break;
            
            case 'pindahdomisili':
                return ['param'=>'pindahdomisili',
                            'include'=>'pindahdomisili.php',
                            'msg'=>'Pendataan Penduduk Pindah Domisili',
                            'where'=>['parameter'=>'pindahdomisili']
                        ];
                break;
            
            
            case 'menikah':
                return ['param'=>'menikah',
                            'include'=>'menikah.php',
                            'msg'=>'Pendataan Penduduk yang Menikah ',
                            'where'=>['parameter'=>'menikah']
                        ];
                break;
            
            
            case 'cerai':
                return ['param'=>'cerai',
                            'include'=>'cerai.php',
                            'msg'=>'Pendataan Penduduk yang Cerai ',
                            'where'=>['parameter'=>'cerai']
                        ];
                break;
            
            default:
                return ['param'=>null,
                            'include'=>'penduduk.php',
                            'msg'=>'',
                            'where'=>['jenis'=>'status']
                        ];
                # code...
                break;
        }
    }
        
    function bulanList(){
        $op=array();
        $op['']='--Semua Bulan--';
        for ($i=1; $i <=12 ; $i++) {
            if(strlen($i)==1){
                $i='0'.$i;
            } 
            $op[$i]=bulan_huruf($i);
        }
        return $op;
    }
    function tahunList($a='',$b=''){
        if($a==''){
            $a=date('Y')-5;
        }
        if($b==''){
            $b=$a+10;
        }
        $op=array();
        $op['']='--Semua Tahun--';
        for ($i=$a; $i <=$b ; $i++) { 
            $op[$i]=$i;
        }
        return $op;
    }
    function setList($a,$b,$c){
        $op=array();
        $op['']='--Pilih Semua--';
        foreach ($a as $r) {
            $op[$r->$b]=$r->$c;
        }
        return $op;
    }
	function request_all($fillable){
		foreach ($fillable as $key => $value) {
	      if(isset($_POST[$value])){
	        $data[$value]=$_POST[$value];
	      }
	    }
		return $data;
	}