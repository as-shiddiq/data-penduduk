<?php

	function anchor2($a,$b,$c='',$d=''){
		if($c!=''){
			$c=' class="'.$c.'"';
		}
		if($d!=''){
			$d=' '.$d;
		}
		return '<a href="'.$a.'"'.$c.$d.'>'.$b.'</a>';
	}
	function icon($a){
		return '<i class="fa fa-'.$a.'"></i>';
	}
	function italic($a,$b=''){
		if($b!=''){
			$b=' class="'.$b.'"';
		}
		return '<i'.$b.'>'.$a.'</i>';
	}

	function small($a,$b='',$c=''){
		if($b!=''){
			$b=' class="'.$b.'"';
		}
		if($c!=''){
			$c=' '.$c.'"';
		}
		return '<small'.$b.$c.'>'.$a.'</small>';
	}

	function span($a,$b='',$c=''){
		if($b!=''){
			$b=' class="'.$b.'"';
		}
		if($c!=''){
			$c=' '.$c.'"';
		}
		return '<span'.$b.$c.'>'.$a.'</span>';
	}

	function div($a,$b='',$c=''){
		if($b!=''){
			$b=' class="'.$b.'"';
		}
		if($c!=''){
			$c=' '.$c.'"';
		}
		return '<div'.$b.$c.'>'.$a.'</div>';
	}

	function thumbnail($str){
		$img=NULL;
		$img[1]=NULL;
		$doc = new DOMDocument();
		@$doc->loadHTML($str);

		$tags = $doc->getElementsByTagName('img');
		$i=1;
		if(count($tags)>0){
			foreach ($tags as $tag) {
			       $img[$i]= $tag->getAttribute('src');
			}
		}
		return $img[1];
	}



	function auto_code($tbl_name,$code='',$length='',$pos=''){
		$nfw=&get_instance();
		$q="SELECT * FROM ".$tbl_name." ORDER BY 1 DESC";
	    $r=$nfw->db->query($q);
	    if($length==''){
	    	$length=3;
	    }
	    if($pos==''){
	    	$pos='0';
	    }
		$generate='';
	    if($r->num_rows()>0){
					$row=$nfw->db->query($q)->row();
					$f=$nfw->db->field_data($tbl_name);
	        $length_code=strlen($code);
					$namef=$f[$pos]->name;
	        $numb=substr($row->$namef,$length_code);
	        $numb=$numb+1;
					// echo $numb;
					$length_numb=strlen($numb);
					if($length_numb!=$numb){
						$loop=$length-$length_numb;
						$def='0';
						for($i=0;$i<$loop;$i++){
							$generate.=$def;
						}
					}
					$value=$generate.$numb;

					$auto_code=$code.$value;
	    }
	    else{
			$loop=$length-1;
			$def='0';
	    	for($i=0;$i<$loop;$i++){
				$generate.=$def;
			}
			$value=$generate.'1';

			$auto_code=$code.$value;
	   	}
	   	return $auto_code;

	}
	function parsing_title($a,$b=''){
		$nama_guru='';
		$ex=explode(',', $a);
        for($i=0;$i<count($ex);$i++){
            if($i==0){
            	if($b=='ucwords'){
	                $nama_guru.=ucwords($ex[$i]);
            	}
            	else{
                	$nama_guru.=strtoupper($ex[$i]);
            	}
                if(count($ex)>1){
                	$nama_guru.=',';
                }
            }
            else{
                $ex2=explode('.', $ex[$i]);
                $nama_guru.=strtoupper($ex2[0]).'. ';
           		$nama_guru.=ucfirst($ex2[1]);
                if(count($ex)<($i+1)){
                	$nama_guru.=', ';
                }
           	}

        }
        return $nama_guru;
	}
	function parsing_nip($a){
		$nip='';
		$nip.=substr($a, 0,8).' ';
		$nip.=substr($a, 8,6 ).' ';
		$nip.=substr($a, 14,1).' ';
		$nip.=substr($a, 15,3);
		return $nip;
	}
