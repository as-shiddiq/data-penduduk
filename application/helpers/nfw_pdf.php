<?php
	use Dompdf\Dompdf;
	use Dompdf\Options;
	function generate_pdf($html='',$nama_file='',$paper='',$orient=''){

	  if($nama_file==''){
	  	$nama_file='report';
	  }
	  if($paper==''){
	  	$paper='legal';
	  }
	  if($orient==''){
	  	$orient='landscape';
	  }

	$options = new Options();
	$options->setIsRemoteEnabled(true);
	$dompdf = new Dompdf($options);
	// $dompdf = new Dompdf();
	$dompdf->loadHtml($html);

	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper($paper,$orient);

	// Render the HTML as PDF
	$dompdf->render();

	// Output the generated PDF to Browser
	// $dompdf->stream();
	$dompdf->stream($nama_file.".pdf", array("Attachment" => false));


}
